module.exports = {
  icon: true,
  titleProp: true,
  template(
    { template },
    opts,
    { imports, componentName, props, jsx, exports },
  ) {
    const typeScriptTpl = template.smart({ plugins: ["typescript"] })
    return typeScriptTpl.ast`
    import { useId } from "@reach/auto-id"
    import * as React from "react"
    type IconProps = React.SVGProps<SVGSVGElement> & {
      title?: string
      titleId?: string
    }
    const ${componentName} = React.forwardRef(({title, titleId, ...props}: IconProps, svgRef: React.Ref<SVGSVGElement>) => {
      titleId = useId(titleId)

      return (
        ${jsx}
      )
    })
    export default ${componentName}
  `
  },
}
