const prod = process.env.NODE_ENV === "production"

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    public: "/",
    src: "/_dist_",
  },
  plugins: [
    "@snowpack/plugin-react-refresh",
    "@snowpack/plugin-dotenv",
    "@snowpack/plugin-typescript",
    ...(prod
      ? [
          [
            "snowpack-plugin-webpack5",
            {
              sourceMap: true,
            },
          ],
        ]
      : []),
  ],

  routes: [
    /* Enable an SPA Fallback in development: */
    { match: "routes", src: ".*", dest: "/index.html" },
  ],
  packageOptions: {
    polyfillNode: true,
  },
  devOptions: {
    port: 3070,
    open: "none",
  },
  buildOptions: {
    clean: true,
  },
  alias: {
    "@app": "./src",
  },
}
