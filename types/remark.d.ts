declare module "remark-parse/index" {
  import remark from "remark-parse"
  export default remark
}
declare module "unified/index" {
  import unified from "unified"
  export default unified
}
