import {
  ReactElement,
  ReactNode,
  createContext,
  createElement,
  useCallback,
  useContext,
  useLayoutEffect,
  useRef,
  useState,
} from "react"

export type Value =
  | object
  | symbol
  | string
  | number
  | boolean
  | null
  | undefined

export abstract class ChangeNotifier {
  protected listeners = new Set<() => void>()

  get hasListeners() {
    return this.listeners.size > 0
  }

  protected notifyListeners() {
    for (const listener of this.listeners) {
      listener()
    }
  }

  public subscribe(listener: () => void) {
    this.listeners.add(listener)

    return () => {
      this.listeners.delete(listener)
    }
  }
}

export abstract class Store<T> extends ChangeNotifier {
  protected state: T
  protected inBatch = false

  constructor(initialState: T) {
    super()
    this.state = initialState
  }

  public getState() {
    return this.state
  }

  protected setState(state: T) {
    this.state = state
    if (!this.inBatch) {
      this.notifyListeners()
    }
  }

  protected batch(executor: () => void) {
    if (this.inBatch) return executor()

    try {
      this.inBatch = true
      executor()
    } finally {
      this.inBatch = false
      this.notifyListeners()
    }
  }
}

export type StoreContext<TState, TStore extends Store<TState>> = {
  Provider: (props: { store: TStore; children: ReactNode }) => ReactElement
  useStore: () => TStore
  useStateSlice: <U>(
    getSlice: (state: TState) => U,
    options?: {
      deps?: Array<unknown>
      isEqual?: (
        olwSlice: U,
        newSlice: U,
        oldState: TState,
        newState: TState,
      ) => boolean
    },
  ) => U
}

const defaultIsEqual = <T>(oldSlice: T, newSlice: T) => {
  return oldSlice === newSlice
}
const initialRefValue = Symbol() as any

export function createStoreContext<TStore extends Store<any>>(): StoreContext<
  ReturnType<TStore["getState"]>,
  TStore
> {
  const context = createContext<TStore>(undefined as any)

  return {
    Provider: props =>
      createElement(context.Provider, {
        value: props.store,
        children: props.children,
      }),
    useStore: () => {
      const store = useContext(context)

      if (store === undefined) {
        throw Error("StoreContext used without a provider")
      }

      return store
    },
    useStateSlice(getSlice, { deps = [], isEqual = defaultIsEqual } = {}) {
      const store = useContext(context)

      if (store === undefined) {
        throw Error("StoreContext used without a provider")
      }

      const memoedGetSlice = useCallback(getSlice, deps)
      const memoedGetSliceRef = useRef(memoedGetSlice)
      const currentState = store.getState()
      const stateRef = useRef(currentState)
      const sliceRef = useRef<ReturnType<typeof getSlice>>(initialRefValue)

      // Update the value on rerender if getSlice changed
      // We do this in the render step and not with useEffect for two reasons:
      //   1. We want the update to happen the same render and not on the one after
      //   2. We do not want to schedule a second render for performance reasons
      if (
        sliceRef.current === initialRefValue ||
        memoedGetSliceRef.current !== memoedGetSlice
      ) {
        memoedGetSliceRef.current = memoedGetSlice
        stateRef.current = currentState
        sliceRef.current = memoedGetSlice(currentState)
      }

      // We use useState only to have a way of triggering a rerender on store
      // updates, the real state is kept in useRef for the above reasons
      const [, rerender] = useState<any>()
      useLayoutEffect(
        () =>
          store.subscribe(() => {
            const newState = store.getState()
            const newSlice = memoedGetSlice(newState)

            if (
              !isEqual(sliceRef.current, newSlice, stateRef.current, newState)
            ) {
              stateRef.current = newState
              sliceRef.current = newSlice
              rerender({})
            }
          }),
        [store, memoedGetSlice, isEqual],
      )

      if (import.meta.env.MODE === "development") {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const didWarnRef = useRef(false)

        if (didWarnRef.current === false) {
          const stabilityCheck = memoedGetSlice(stateRef.current)

          if (
            !isEqual(
              sliceRef.current,
              stabilityCheck,
              stateRef.current,
              stateRef.current,
            )
          ) {
            console.warn(
              `getSlice function provided to useStateSlice does not return a stable value, this will cause a lot of unnecessary rerenders. ${
                isEqual === defaultIsEqual
                  ? "Provide an isEqual function to check for equality if you need to return a new object every time"
                  : "The provided isEqual function should ensure that isEqual(old, new) === false only is valid when the state actually have changed"
              }`,
            )
            console.warn(Error())
            didWarnRef.current = true
          }
        }
      }

      return sliceRef.current
    },
  }
}

export interface PersistableStore<TState, TPersistedData = TState>
  extends Store<TState> {
  getStateToPersist?(): TPersistedData

  onLoaded(
    result:
      | { type: "empty" }
      | { type: "success"; version: number; data: TPersistedData }
      | { type: "error"; error: any },
  ): void
}

type LocalStorageData = { version: number; state: any }

export function persistStore<TState, TPersistedData = TState>(
  store: PersistableStore<TState, TPersistedData>,
  options: {
    key: string
    version: number
  },
) {
  let haveLoadedData = false

  setTimeout(() => {
    try {
      const json = localStorage.getItem(options.key)
      if (json) {
        const data: LocalStorageData = JSON.parse(json)
        if (
          typeof data.version !== "number" ||
          !data.state ||
          typeof data.state !== "object"
        ) {
          throw Error("Invalid storage format")
        }
        store.onLoaded({
          type: "success",
          version: data.version,
          data: data.state,
        })
        haveLoadedData = true
      } else {
        store.onLoaded({
          type: "empty",
        })
        haveLoadedData = true
      }
    } catch (error) {
      console.error(`Error loading stored data for key ${options.key}`, error)
      store.onLoaded({
        type: "error",
        error,
      })
      haveLoadedData = true
    }
  })

  let previousStoredState: any = undefined

  const getState = store.getStateToPersist
    ? () => store.getStateToPersist!()
    : () => store.getState()

  store.subscribe(() => {
    if (haveLoadedData) {
      const state = getState()

      if (state === previousStoredState) return

      try {
        const data: LocalStorageData = { state, version: options.version }
        localStorage.setItem(options.key, JSON.stringify(data))
        previousStoredState = state
      } catch (err) {
        console.error(`Error saving data for key ${options.key}`, err)
      }

      return state
    }
  })
}
