import * as React from "react"
import { useEffect, useRef, useState } from "react"
import {
  BrowserRouter,
  Link,
  Route,
  Routes,
  useNavigate,
  useParams,
} from "react-router-dom"
import { ThemeProvider } from "styled-components"
import { BoardPage } from "./pages/BoardPage/BoardPage"
import { RedirectIssuePage } from "./pages/IssuePage/RedirectIssuePage"
import { SignInPage } from "./pages/SignInPage"
import { BoardStore } from "./stores/BoardStore"
import { ProjectStore, projectContext } from "./stores/ProjectStore"
import { createRedmineStore, redmineContext } from "./stores/RedmineStore"
import { darkTheme } from "./styles"

export const App = () => {
  const [redmineStore] = useState(createRedmineStore)

  return (
    <React.StrictMode>
      <ThemeProvider theme={darkTheme}>
        <redmineContext.Provider store={redmineStore}>
          <Main />
        </redmineContext.Provider>
      </ThemeProvider>
    </React.StrictMode>
  )
}

const Main = () => {
  const redmineStore = redmineContext.useStore()
  const settingsLoaded = redmineContext.useStateSlice(r => r.settingsLoaded)
  const apiSettings = redmineContext.useStateSlice(r => r.settings)
  const redmineData = redmineContext.useStateSlice(r => r.data)
  const redmineDataLoadingRef = useRef(false)

  const isLoggedIn = apiSettings.baseUrl && apiSettings.apiKey

  useEffect(() => {
    if (!isLoggedIn || redmineData || redmineDataLoadingRef.current) return
    redmineDataLoadingRef.current = true

    const { api } = redmineStore.getState()
    Promise.all([
      api.fetchPriorities(),
      api.fetchProjects(),
      api.fetchStatuses(),
      api.fetchTrackers(),
    ] as const).then(([priorities, projects, statuses, trackers]) => {
      redmineStore.setRedmineData({ priorities, projects, statuses, trackers })
    })
  }, [isLoggedIn, redmineData, redmineStore])

  if (!settingsLoaded) return null

  return isLoggedIn ? (
    redmineData ? (
      <BrowserRouter>
        <Routes basename={import.meta.env.PUBLIC_URL}>
          <Route path="/project/:projectSlug" element={<ProjectPage />} />
          <Route
            path="/project/:projectSlug/:versionName/*"
            element={<ProjectPage />}
          />
          <Route path="/issue/:issueId" element={<RedirectIssuePage />} />
          <Route path="/" element={<SelectProjectPage />} />
        </Routes>
      </BrowserRouter>
    ) : null
  ) : (
    <SignInPage />
  )
}

export const SelectProjectPage = () => {
  const projects = redmineContext.useStateSlice(
    api =>
      api.data?.projects.sort((a, b) => a.name.localeCompare(b.name)) ?? [],
    { isEqual: (_a, _b, a, b) => a?.data?.projects === b?.data?.projects },
  )

  return (
    <div>
      <h1 style={{ margin: 32 }}>Projects</h1>
      <div style={{ display: "grid", gridGap: 8, margin: 32 }}>
        {projects.map(project => (
          <Link
            key={project.id}
            to={`/project/${encodeURIComponent(project.slug)}`}
            style={{ textDecoration: "none" }}
          >
            {project.name}
          </Link>
        ))}
      </div>
    </div>
  )
}

export const ProjectPage = () => {
  const routeParams = useParams() as Record<string, string>
  const redmineStore = redmineContext.useStore()
  const projectStore = redmineContext.useStateSlice(r => r.projectStore)
  const currentlyLoadingProjectRef = useRef("")
  const currentlyLoadingVersionRef = useRef("")
  const navigate = useNavigate()

  if (!redmineStore.getState().data) {
    throw Error("Redmine data must be loaded before <ProjectPage> is rendered")
  }

  const projectSlug = decodeURIComponent(routeParams.projectSlug)
  const versionName = decodeURIComponent(routeParams.versionName)

  useEffect(() => {
    const redmineState = redmineStore.getState()
    const selectedProjectSlug = redmineState.projectStore?.getState().slug

    if (
      selectedProjectSlug === projectSlug ||
      currentlyLoadingProjectRef.current === projectSlug
    ) {
      return
    }

    currentlyLoadingProjectRef.current = projectSlug
    if (selectedProjectSlug) {
      redmineStore.setProject(undefined)
    }

    redmineState.api
      .fetchProject(redmineState.data!, projectSlug)
      .then(project => {
        if (currentlyLoadingProjectRef.current !== projectSlug) return
        currentlyLoadingProjectRef.current = ""

        const projectStore = new ProjectStore(redmineStore, project)
        redmineStore.setProject(projectStore)
      })
  }, [navigate, redmineStore, projectSlug, versionName])

  useEffect(() => {
    if (!projectStore) return
    const redmineState = redmineStore.getState()
    const projectState = projectStore.getState()
    const selectedVersionName = projectState.boardStore?.getState().version.name

    if (
      selectedVersionName === versionName ||
      currentlyLoadingVersionRef.current === versionName
    ) {
      return
    }

    currentlyLoadingVersionRef.current = versionName
    if (selectedVersionName) {
      projectStore.setBoard(undefined)
    }
    const version = projectState.versions.find(v => v.name === versionName)
    if (!version) return

    redmineState.api.fetchBoard(projectState, version).then(board => {
      if (currentlyLoadingVersionRef.current !== versionName) return
      currentlyLoadingVersionRef.current = ""

      const boardStore = new BoardStore(projectStore, {
        ...board,
        version,
        undoHistory: [],
        fetchedAt: new Date(),
      })
      projectStore.setBoard(boardStore)
    })
  }, [navigate, redmineStore, projectStore, versionName])

  return projectStore?.getState().slug === projectSlug ? (
    <projectContext.Provider store={projectStore}>
      <BoardPage />
    </projectContext.Provider>
  ) : null
}
