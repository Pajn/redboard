import { expect } from "@esm-bundle/chai"
import { Map } from "immutable"
import type { Issue } from "../types"
import { StructureUtils } from "./BoardStore"

const issue: Issue = {
  id: 1,
  projectId: 1,
  subject: "issue",
  description: "",
  tracker: {
    id: 1,
    name: "Task",
  },
  status: {
    id: 1,
    name: "New",
  },
  customFields: [],
  attachments: [],

  priority: {
    id: 1,
    name: 'normal'
  },
  createdBy: {
    id: 1,
    name: 'user'
  },
  createdOn: new Date(0),
  updatedOn: new Date(0),
}

describe("BoardStore", () => {
  describe("StructureUtils", () => {
    describe("addToSwimlane", () => {
      it("should create a new swimlane if one did not exist", () => {
        const parentIssue = issue
        const childIssue = { ...issue, id: 2, parent: parentIssue.id }

        const result = StructureUtils.addToSwimlane([], parentIssue, childIssue)
        expect(result.length).to.equal(1)
        expect(result[0][0]).to.equal(parentIssue)
        expect([...result[0][1].entries()]).to.deep.equal([
          [childIssue.status.name, [childIssue.id]],
        ])
      })

      it("should add to the swimlane if one did exist", () => {
        const parentIssue = issue
        const childIssue = { ...issue, id: 2, parent: parentIssue.id }
        const otherIssueId = 3

        const result = StructureUtils.addToSwimlane(
          [[parentIssue, Map([[childIssue.status.name, [otherIssueId]]])]],
          parentIssue,
          childIssue,
        )
        expect(result.length).to.equal(1)
        expect(result[0][0]).to.equal(parentIssue)
        expect([...result[0][1].entries()]).to.deep.equal([
          [childIssue.status.name, [otherIssueId, childIssue.id]],
        ])
      })

      it("should add at the specified index", () => {
        const parentIssue = issue
        const childIssue = { ...issue, id: 2, parent: parentIssue.id }
        const otherIssueId = 3

        const result = StructureUtils.addToSwimlane(
          [[parentIssue, Map([[childIssue.status.name, [otherIssueId]]])]],
          parentIssue,
          childIssue,
          0,
        )
        expect(result.length).to.equal(1)
        expect(result[0][0]).to.equal(parentIssue)
        expect([...result[0][1].entries()]).to.deep.equal([
          [childIssue.status.name, [childIssue.id, otherIssueId]],
        ])
      })
    })

    describe("removeFromSwimlane", () => {
      it("should not do anything if the parent does not exist", () => {
        const parentIssue = issue
        const childIssue = { ...issue, id: 2, parent: parentIssue.id }

        const result = StructureUtils.removeFromSwimlane(
          [],
          parentIssue.id,
          childIssue,
        )
        expect(result.length).to.equal(0)
      })

      it("should not do anything if the parent does not contain the child", () => {
        const parentIssue = issue
        const childIssue = { ...issue, id: 2, parent: parentIssue.id }

        const result = StructureUtils.removeFromSwimlane(
          [[parentIssue, Map([[childIssue.status.name, []]])]],
          parentIssue.id,
          childIssue,
        )
        expect(result.length).to.equal(1)
        expect(result[0][0]).to.equal(parentIssue)
        expect([...result[0][1].entries()]).to.deep.equal([
          [childIssue.status.name, []],
        ])
      })

      it("should remove the issue", () => {
        const parentIssue = issue
        const childIssue = { ...issue, id: 2, parent: parentIssue.id }

        const result = StructureUtils.removeFromSwimlane(
          [[parentIssue, Map([[childIssue.status.name, [childIssue.id]]])]],
          parentIssue.id,
          childIssue,
        )
        expect(result.length).to.equal(1)
        expect(result[0][0]).to.equal(parentIssue)
        expect([...result[0][1].entries()]).to.deep.equal([
          [childIssue.status.name, []],
        ])
      })
    })
  })
})
