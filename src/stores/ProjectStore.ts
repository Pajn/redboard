import { Store, createStoreContext } from "../store"
import type { Project } from "../types"
import type { BoardStore } from "./BoardStore"
import type { RedmineStore } from "./RedmineStore"

export type ProjectState = Project & {
  boardStore?: BoardStore
}

export const projectContext = createStoreContext<ProjectStore>()

export class ProjectStore extends Store<ProjectState> {
  constructor(readonly redmineStore: RedmineStore, project: Project) {
    super(project)
  }

  setBoard(boardStore: BoardStore | undefined) {
    this.setState({ ...this.state, boardStore })
  }
}
