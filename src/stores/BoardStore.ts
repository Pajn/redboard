import { Map } from "immutable"
import { ArrayUtils } from "../arrayUtils"
import { Store, createStoreContext } from "../store"
import type {
  Board,
  BoardStructure,
  Clearable,
  CustomField,
  Issue,
  IssueId,
  NewIssue,
  Status,
  SwimlaneColumns,
  Version,
} from "../types"
import type { ProjectStore } from "./ProjectStore"

export type BoardState = Board & {
  version: Version
  undoHistory: Array<UndoEvent>
  fetchedAt: Date
}

export type UndoEvent = {
  id: number
  description: string
  issue: Issue
  undoAction: {
    issueId: number
    modifications: Array<IssueAction>
  }
}

export type IssueAction =
  | {
      type: "setStatus"
      status: Pick<Status, "id" | "name">
      index: number | undefined
    }
  | {
      type: "setParentIssue"
      parent: number | undefined
      index: number | undefined
    }
  | {
      type: "setVersion"
      version: Pick<Version, "id" | "name"> | undefined
    }
  | { type: "reorder"; fromIndex: number; toIndex: number }
  | {
      type: "setFields"
      fields: Partial<
        Clearable<
          Pick<Issue, "tracker" | "subject" | "description" | "assignee">
        >
      >
    }
  | {
      type: "setCustomFields"
      fields: Array<Pick<CustomField, "name" | "value">>
    }

export const boardContext = createStoreContext<BoardStore>()

export class BoardStore extends Store<BoardState> {
  constructor(readonly projectStore: ProjectStore, initialState: BoardState) {
    super(initialState)
    ;(window as any).boardStore = this
  }

  async refresh() {
    const { api } = this.projectStore.redmineStore.getState()
    const project = this.projectStore.getState()
    const { version } = this.state
    const board = await api.fetchBoard(project, version)
    this.setState({
      ...this.state,
      ...board,
      fetchedAt: new Date(),
    })
  }

  async createIssue(issue: NewIssue) {
    const {
      api,
      settings: { swimlaneTrackers },
    } = this.projectStore.redmineStore.getState()
    const createdIssue = await api.createIssue(issue)

    let structure = this.state.structure

    if (swimlaneTrackers.includes(createdIssue.tracker.name)) {
      structure = structure.concat([[createdIssue, Map()]])
    }

    if (createdIssue.parent) {
      let parentIssue = this.state.issues.get(createdIssue.parent) ?? null
      if (!parentIssue) {
        parentIssue = await api.fetchIssue(createdIssue.parent)
      }

      if (parentIssue) {
        structure = StructureUtils.addToSwimlane(
          structure,
          parentIssue,
          createdIssue,
        )
      }
    }

    this.setState({
      ...this.state,
      structure,
      issues: this.state.issues.set(createdIssue.id, createdIssue),
    })

    return createdIssue
  }

  updateIssue(action: {
    issueId: number
    comment?: string
    modifications: Array<IssueAction>
  }): Promise<void> {
    const oldState = this.state
    if (this.state.issues.has(action.issueId)) {
      this.setState(
        action.modifications.reduce(
          (board, issueAction) =>
            issueReducer(this.projectStore, board, action.issueId, issueAction),
          this.state,
        ),
      )
    } else {
      console.warn(`Updating missing issue id ${action.issueId}`)
    }

    const changes: Partial<Clearable<Issue>> = {}
    action.modifications.forEach(modification => {
      switch (modification.type) {
        case "setStatus": {
          changes.status = modification.status
          break
        }
        case "setParentIssue": {
          changes.parent = modification.parent ?? null
          break
        }
        case "setVersion": {
          changes.version = modification.version ?? null
          break
        }
        case "setFields": {
          Object.assign(changes, modification.fields)
          break
        }
        case "setCustomFields": {
          const issue = this.state.issues.get(action.issueId)
          if (issue) {
            changes.customFields = issue.customFields
          }
        }
      }
    })

    if (action.comment || Object.keys(changes).length > 0) {
      const { api } = this.projectStore.redmineStore.getState()
      return api
        .updateIssue(action.issueId, action.comment, changes)
        .catch(error => {
          this.setState(oldState)

          throw error
        })
    } else {
      return Promise.resolve()
    }
  }

  undo(event: UndoEvent) {
    this.batch(() => {
      this.updateIssue(event.undoAction)
      this.setState({
        ...this.state,
        undoHistory: this.state.undoHistory.filter(e => e.id !== event.id),
      })
    })
  }
}

export const StructureUtils = {
  addToSwimlane: (
    structure: BoardStructure,
    parentIssue: Issue,
    childIssue: Issue,
    index?: IssueId,
  ) =>
    ArrayUtils.upsertWhere(
      structure,
      ([p]) => p.id === parentIssue.id,
      [parentIssue, Map() as SwimlaneColumns] as const,
      ([swimlane, columns]) =>
        [
          swimlane,
          columns.update(childIssue.status.name, (issues = []) =>
            ArrayUtils.insert(issues, index ?? issues.length, childIssue.id),
          ),
        ] as const,
    ),
  removeFromSwimlane: (
    structure: BoardStructure,
    parentIssueId: IssueId,
    childIssue: Issue,
  ) =>
    ArrayUtils.updateWhere(
      structure,
      ([p]) => p.id === parentIssueId,
      ([swimlane, columns]) =>
        [
          swimlane,
          columns.update(childIssue.status.name, (issues = []) =>
            issues.filter(t => t !== childIssue.id),
          ),
        ] as const,
    ),
  updateSwimlane: (
    structure: BoardStructure,
    parentIssueId: IssueId,
    updater: (columns: SwimlaneColumns) => SwimlaneColumns,
  ) =>
    ArrayUtils.updateWhere(
      structure,
      ([swimlane]) => swimlane.id === parentIssueId,
      ([swimlane, columns]) => [swimlane, updater(columns)] as const,
    ),
}

let nextHistoryEventId = 1

const issueReducer = (
  projectStore: ProjectStore,
  board: BoardState,
  issueId: number,
  action: IssueAction,
): BoardState => {
  switch (action.type) {
    case "reorder": {
      const issue = board.issues.get(issueId)!
      if (!issue.parent) return board

      return {
        ...board,
        structure: StructureUtils.updateSwimlane(
          board.structure,
          issue.parent,
          columns =>
            columns.update(issue.status.name, issues =>
              ArrayUtils.move(issues, action.fromIndex, action.toIndex),
            ),
        ),
      }
    }
    case "setStatus": {
      const issue = board.issues.get(issueId)!

      let structure = board.structure
      let undoHistory = board.undoHistory

      if (issue.parent) {
        structure = StructureUtils.updateSwimlane(
          structure,
          issue.parent,
          columns =>
            columns.withMutations(columns => {
              columns.update(issue.status.name, (issues = []) =>
                issues.filter(t => t !== issueId),
              )
              columns.update(action.status.name, (issues = []) =>
                ArrayUtils.insert(
                  issues,
                  action.index ?? issues.length,
                  issueId,
                ),
              )
            }),
        )

        const { destructiveStatuses } = projectStore.getState()

        if (destructiveStatuses.some(s => s.id === action.status.id)) {
          undoHistory = board.undoHistory.concat({
            id: nextHistoryEventId++,
            description: action.status.name,
            issue,
            undoAction: {
              issueId,
              modifications: [
                {
                  type: "setStatus",
                  status: issue.status,
                  index: board.structure
                    .find(([swimlane]) => swimlane.id === issue.parent)![1]
                    .get(issue.status.name)!
                    .indexOf(issueId),
                },
              ],
            },
          })
        }
      }

      return {
        ...board,
        structure,
        issues: board.issues.set(issueId, { ...issue, status: action.status }),
        undoHistory,
      }
    }
    case "setParentIssue": {
      const issue = board.issues.get(issueId)!
      const parentIssue = action.parent && board.issues.get(action.parent)
      if (!parentIssue && action.parent) {
        throw Error(`Parent issue ${action.parent} does not exists`)
      }

      let structure = board.structure

      if (issue.parent) {
        structure = StructureUtils.removeFromSwimlane(
          structure,
          issue.parent,
          issue,
        )
      }

      if (parentIssue) {
        structure = StructureUtils.addToSwimlane(
          structure,
          parentIssue,
          issue,
          action.index,
        )
      }

      return {
        ...board,
        structure,
        issues: board.issues.set(issueId, {
          ...issue,
          parent: action.parent,
        }),
      }
    }
    case "setVersion": {
      const issue = board.issues.get(issueId)!
      const parentIssue = issue.parent && board.issues.get(issue.parent)
      if (issue.version?.name === action.version?.name) return board

      let structure = board.structure
      let undoHistory = board.undoHistory

      // Changed from board version
      if (issue.version?.name === board.version.name && issue.parent) {
        structure = StructureUtils.removeFromSwimlane(
          structure,
          issue.parent,
          issue,
        )
        undoHistory = undoHistory.concat({
          id: nextHistoryEventId++,
          description: "Changed version of",
          issue,
          undoAction: {
            issueId,
            modifications: [
              {
                type: "setVersion",
                version: issue.version,
              },
            ],
          },
        })
      }

      // Changed to board version
      if (action.version?.name === board.version.name && parentIssue) {
        structure = StructureUtils.addToSwimlane(
          board.structure,
          parentIssue,
          issue,
        )
      }

      return {
        ...board,
        structure,
        undoHistory,
        issues: board.issues.set(issueId, {
          ...issue,
          version: action.version ?? undefined,
        }),
      }
    }
    case "setFields": {
      const issue = { ...board.issues.get(issueId)! }
      for (const [key, value] of Object.entries(action.fields)) {
        ;(issue as any)[key] = (value ?? undefined) as any
      }
      return {
        ...board,
        issues: board.issues.set(issueId, issue),
      }
    }
    case "setCustomFields": {
      const issue = { ...board.issues.get(issueId)! }
      for (const field of action.fields) {
        issue.customFields = ArrayUtils.updateWhere(
          issue.customFields,
          f => f.name === field.name,
          f => ({ ...f, ...field }),
        )
      }
      return {
        ...board,
        issues: board.issues.set(issueId, issue),
      }
    }
  }
}
