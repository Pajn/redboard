import { ApiClient, ApiSettings, createApiClient } from "@app/api/api"
import {
  PersistableStore,
  Store,
  createStoreContext,
  persistStore,
} from "@app/store"
import type { Priority, Status, Tracker } from "@app/types"
import type { ProjectStore } from "./ProjectStore"

export type RedmineState = {
  settingsLoaded: boolean
  api: ApiClient
  settings: ApiSettings
  data?: {
    priorities: Array<Priority>
    projects: Array<{ id: number; name: string; slug: string }>
    statuses: Array<Status>
    trackers: Array<Tracker>
  }
  projectStore?: ProjectStore
}

export function createRedmineStore() {
  const api = createApiClient(() => redmineStore.getState().settings)

  const redmineStore = new RedmineStore(api)
  persistStore(redmineStore, {
    key: "redmineApi",
    version: 3,
  })

  return redmineStore
}

export const redmineContext = createStoreContext<RedmineStore>()

export class RedmineStore extends Store<RedmineState>
  implements PersistableStore<RedmineState, ApiSettings> {
  constructor(api: ApiClient) {
    super({
      api,
      settingsLoaded: false,
      settings: {
        customMemberFields: ["Reviewer", "Tester"],
        swimlaneTrackers: ["Story", "Feature"],
        hiddenStatuses: ["Deployed", "Feedback"],
      },
    })
  }

  getStateToPersist(): ApiSettings {
    return this.state.settings
  }

  onLoaded(
    result:
      | { type: "empty" }
      | { type: "error"; error: any }
      | {
          type: "success"
          version: number
          data: any
        },
  ): void {
    const newState = { ...this.state, settingsLoaded: true }
    if (result.type === "success") {
      const versioned = result as
        | {
            type: "success"
            version: 1
            data: { settings: ApiSettings }
          }
        | {
            type: "success"
            version: 2
            data: Omit<ApiSettings, "customMemberFields">
          }
        | {
            type: "success"
            version: 3
            data: ApiSettings
          }
      if (versioned.version === 1) {
        newState.settings = {
          ...this.state.settings,
          ...versioned.data.settings,
        }
      } else if (versioned.version <= 3) {
        newState.settings = { ...this.state.settings, ...versioned.data }
      }
    }
    this.setState(newState)
  }

  formatIssueUrl(issueId: number) {
    return new URL(`/issues/${issueId}`, this.state.settings.baseUrl).href
  }

  setBaseUrl(baseUrl: string) {
    this.setState({
      ...this.state,
      settings: { ...this.state.settings, baseUrl },
    })
  }
  setApiKey(apiKey: string) {
    this.setState({
      ...this.state,
      settings: { ...this.state.settings, apiKey },
    })
  }
  setRedmineData(data: RedmineState["data"]) {
    this.setState({ ...this.state, data })
  }
  setProject(projectStore: ProjectStore | undefined) {
    this.setState({ ...this.state, projectStore })
  }
}
