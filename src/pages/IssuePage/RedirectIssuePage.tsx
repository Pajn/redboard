import { redmineContext } from "@app/stores/RedmineStore"
import { useEffect } from "react"
import { useNavigate, useParams } from "react-router-dom"

export const RedirectIssuePage = () => {
  const navigate = useNavigate()
  const issueId: string | undefined = useParams().issueId

  const apiClient = redmineContext.useStateSlice(state => state.api)

  useEffect(() => {
    if (issueId && !isNaN(+issueId)) {
      apiClient.fetchIssue(+issueId).then(async issue => {
        if (issue && issue.version) {
          const project = await apiClient.fetchSimpleProject(issue.projectId)
          navigate(
            `/project/${decodeURIComponent(
              project.identifier,
            )}/${encodeURIComponent(issue.version.name)}/${issueId}`,
            { replace: true },
          )
        }
      })
    }
  }, [issueId, apiClient, navigate])

  return null
}
