import { boardContext } from "@app/stores/BoardStore"
import { projectContext } from "@app/stores/ProjectStore"
import { redmineContext } from "@app/stores/RedmineStore"
import { IconButton, Input, Label, LinkButton } from "@app/styles"
import type { Issue } from "@app/types"
import { LoadingSpinner } from "@app/ui/LoadingSpinner"
import { Popper } from "@app/ui/Popper"
import EditIcon from "@app/ui/icons/Pencil"
import { useCurrentValue } from "@app/ui/transitions/utils"
import type * as PopperJs from "@popperjs/core"
import offset from "@popperjs/core/lib/modifiers/offset"
import Downshift from "downshift"
import { useEffect, useState } from "react"
import * as React from "react"
import { useTheme } from "styled-components"

export const IssuePicker = (props: {
  label: string
  value?: string
  onChange?: (value: number | undefined) => void
  filterIssues?: Array<number>
  inputRef: React.MutableRefObject<HTMLInputElement | null>
}) => {
  const theme = useTheme()
  const api = redmineContext.useStateSlice((redmine) => redmine.api)
  const storeIssue = boardContext.useStateSlice(
    (s) => (props.value ? s.issues.get(+props.value) : undefined),
    {
      deps: [props.value],
    },
  )
  const [fetchedIssue, setFetchedIssue] = useState<Issue | null>(null)
  const issue = storeIssue ?? fetchedIssue

  const continerRef = React.useRef<HTMLDivElement | null>(null)
  const editButtonRef = React.useRef<HTMLButtonElement | null>(null)
  const popperRef = React.useRef<PopperJs.Instance | null>(null)

  useEffect(() => {
    if (!storeIssue && props.value && !(fetchedIssue?.id === +props.value)) {
      api.fetchIssue(+props.value).then(setFetchedIssue)
    } else if (!props.value && fetchedIssue) {
      setFetchedIssue(null)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.value, fetchedIssue])

  const ParentLink = props.value ? LinkButton : "div"

  return (
    <>
      <Downshift<Issue>
        onChange={(issue) => {
          props.onChange?.(issue?.id ?? undefined)
          editButtonRef.current?.focus()
        }}
        itemToString={(issue) => (issue ? issue.id.toString() : "")}
        selectedItem={issue}
      >
        {({
          getInputProps,
          getItemProps,
          getLabelProps,
          getMenuProps,
          isOpen,
          inputValue,
          highlightedIndex,
          selectedItem,
          openMenu,
          closeMenu,
          clearItems,
          setHighlightedIndex,
        }) => (
          <div
            style={{
              overflow: "hidden",
              width: "100%",
            }}
          >
            <Label {...getLabelProps()}>
              <span>{props.label}</span>
              <div
                ref={continerRef}
                style={{
                  position: "relative",
                  display: "flex",
                  overflow: "hidden",
                  width: "100%",
                  borderRadius: 5,
                  backgroundColor: theme.input.normal,
                }}
              >
                <ParentLink
                  to={props.value ? `../${props.value}` : ""}
                  style={{
                    flex: 1,
                    padding: "4px 8px",
                    overflow: "hidden",
                    borderRadius: 0,
                  }}
                >
                  <div
                    style={{
                      height: "1.2em",
                      paddingBottom: 3,
                      fontSize: "0.9em",
                      lineHeight: 1.2,
                      color: theme.palette.text.secondary,
                    }}
                  >
                    {props.value && "#"}
                    {props.value}
                    {issue && ` - ${issue.tracker.name}`}
                  </div>
                  <div
                    style={{
                      height: "1.2em",
                      overflow: "hidden",
                      lineHeight: 1.2,
                      whiteSpace: "nowrap",
                      textOverflow: "ellipsis",
                    }}
                  >
                    {issue?.subject}
                  </div>
                </ParentLink>
                <IconButton
                  title="Change Parent Issue"
                  ref={editButtonRef}
                  onClick={() => {
                    openMenu()
                    props.inputRef.current?.focus()
                  }}
                  style={{
                    alignSelf: "stretch",
                    margin: 0,
                    width: 40,
                    height: "auto",
                    borderRadius: 0,
                  }}
                  aria-haspopup="true"
                >
                  <EditIcon />
                </IconButton>

                <Input
                  {...(getInputProps({
                    ref: props.inputRef,
                    style: {
                      position: "absolute",
                      height: "100%",
                      opacity: isOpen ? 1 : 0,
                      pointerEvents: isOpen ? "auto" : "none",
                    },
                    tabIndex: isOpen ? 0 : -1,
                    onKeyDown: (e: React.KeyboardEvent) => {
                      if (e.key === "Enter" && !inputValue) {
                        e.stopPropagation()
                        e.preventDefault()
                        props.onChange?.(undefined)
                        closeMenu()
                      } else if (e.key === "Escape") {
                        e.stopPropagation()
                        e.preventDefault()
                      }
                    },
                    onBlur: closeMenu,
                  }) as any)}
                />
              </div>
            </Label>

            <Popper
              open={isOpen}
              anchorEl={continerRef.current}
              placement="bottom-start"
              style={{
                zIndex: 200,
              }}
              modifiers={[
                {
                  ...offset,
                  options: {
                    offset: [0, 4],
                  },
                },
              ]}
              popperRef={popperRef}
            >
              <ul
                {...getMenuProps(
                  {
                    style: {
                      position: "relative",
                      margin: 0,
                      padding: 0,
                      width: 250,
                      listStyle: "none",
                      borderRadius: 5,
                      backgroundColor: theme.palette.background.above,
                      boxShadow: `rgba(0, 0, 0, 0.2) 0px 5px 5px -3px, rgba(0, 0, 0, 0.14) 0px 8px 10px 1px, rgba(0, 0, 0, 0.12) 0px 3px 14px 2px`,
                      overflow: "hidden",
                    },
                  },
                  { suppressRefError: true },
                )}
              >
                {isOpen ? (
                  <FetchIssues
                    searchValue={inputValue ?? ""}
                    limit={10}
                    onLoaded={() => {
                      clearItems()
                      setHighlightedIndex(0)
                      popperRef.current?.update()
                    }}
                    filterIssues={props.filterIssues}
                  >
                    {({ loading, issues, error }) =>
                      loading ? (
                        <div
                          style={{
                            display: "flex",
                            placeContent: "center",
                            height: 24,
                          }}
                        >
                          <LoadingSpinner loading={loading} />
                        </div>
                      ) : error ? (
                        <div>error...</div>
                      ) : (
                        issues.map((issue, index) => (
                          <li
                            {...getItemProps({
                              key: issue.id,
                              index,
                              item: issue,
                              style: {
                                padding: "4px 8px",
                                backgroundColor:
                                  highlightedIndex === index
                                    ? theme.input.focus
                                    : theme.input.normal,
                                fontWeight:
                                  selectedItem === issue ? "bold" : "normal",
                              },
                            })}
                          >
                            <div
                              style={{
                                paddingBottom: 3,
                                fontSize: "0.9em",
                                color: theme.palette.text.secondary,
                              }}
                            >
                              #{issue.id} - {issue.tracker.name}
                            </div>
                            <div
                              style={{
                                whiteSpace: "nowrap",
                                textOverflow: "ellipsis",
                              }}
                            >
                              {issue.subject}
                            </div>
                          </li>
                        ))
                      )
                    }
                  </FetchIssues>
                ) : null}
              </ul>
            </Popper>
          </div>
        )}
      </Downshift>
    </>
  )
}

type FetchIssuesChildProps = {
  loading: boolean
  issues: Array<Issue>
  error: any
}
const FetchIssues = (props: {
  searchValue: string
  onLoaded: () => void
  limit: number
  filterIssues?: Array<number>
  children: (props: FetchIssuesChildProps) => React.ReactNode
}) => {
  const redmineStore = redmineContext.useStore()
  const projectId = projectContext.useStateSlice((p) => p.id)
  const [state, setState] = useState<FetchIssuesChildProps>({
    loading: false,
    issues: [],
    error: undefined,
  })

  const onLoaded = useCurrentValue(props.onLoaded)

  useEffect(() => {
    const { api } = redmineStore.getState()
    if (!props.searchValue) {
      setState({ loading: false, issues: [], error: undefined })
      return
    }
    setState({ loading: true, issues: [], error: undefined })

    api.searchIssues(projectId, props.searchValue).then(
      (issues) => {
        setState({
          loading: false,
          issues: issues
            .filter((i) =>
              props.filterIssues ? !props.filterIssues.includes(i.id) : true,
            )
            .slice(0, props.limit),
          error: undefined,
        })
        onLoaded.current()
      },
      (error) => {
        console.error("FetchIssues error", error)
        setState({ loading: false, issues: [], error })
        onLoaded.current()
      },
    )
  }, [
    onLoaded,
    projectId,
    props.filterIssues,
    props.limit,
    props.searchValue,
    redmineStore,
  ])

  return <>{props.children(state)}</>
}
