import { ApiError } from "@app/api/api"
import { boardContext } from "@app/stores/BoardStore"
import { projectContext } from "@app/stores/ProjectStore"
import { redmineContext } from "@app/stores/RedmineStore"
import {
  Button,
  IconButton,
  IconLinkButton,
  Label,
  Select,
  Textarea,
} from "@app/styles"
import type { FullIssue } from "@app/types"
import { Dialog } from "@app/ui/Dialog"
import ExternalLinkIcon from "@app/ui/icons/ExternalLink"
import EditIcon from "@app/ui/icons/Pencil"
import CloseIcon from "@app/ui/icons/X"
import { useCurrentValue, useFreezableValue } from "@app/ui/transitions/utils"
import { useAutofocus } from "@app/ui/utils"
import * as React from "react"
import { useEffect, useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { useNavigate, useParams } from "react-router-dom"
import { useTheme } from "styled-components"
import { ChildIssues } from "./ChildIssues"
import { Comments } from "./Comments"
import { IssuePicker } from "./IssuePicker"
import {
  IssueFormData,
  PossiblyNewIssue,
  getDefaultValues,
  newIssue,
  saveData,
  submitOnCtrlEnter,
  useDefaultIssue,
} from "./formManagement"

export const IssuePage = () => {
  const theme = useTheme()
  const navigate = useNavigate()
  const currentRawIssueId: string | undefined = useParams().issueId
  const rawIssueId = useFreezableValue(
    currentRawIssueId === undefined,
    currentRawIssueId,
  )
  const isCreatingNewIssue = rawIssueId === newIssue
  const issueId = rawIssueId && !isNaN(+rawIssueId) ? +rawIssueId : undefined

  const redmineStore = redmineContext.useStore()
  const storeIssue = boardContext.useStateSlice(
    (board) => (issueId ? board.issues.get(issueId) : undefined),
    { deps: [issueId] },
  )
  const trackers = projectContext.useStateSlice((project) => project.trackers)
  const priorities = redmineContext.useStateSlice(
    (redmine) => redmine.data!.priorities,
  )
  const statuses = redmineContext.useStateSlice(
    (redmine) => redmine.data!.statuses,
  )
  const members = projectContext.useStateSlice(
    (project) =>
      Array.from(project.members.values()).sort((a, b) =>
        a.name.localeCompare(b.name),
      ),
    { isEqual: (_a, _b, a, b) => a.members === b.members },
  )
  const versions = projectContext.useStateSlice((project) => project.versions)

  const customMemberFields = redmineContext.useStateSlice(
    (s) => s.settings.customMemberFields,
  )

  const [fullIssue, setFullIssue] = useState<FullIssue | null | undefined>(null)

  const defaultIssue = useDefaultIssue()
  const issue: null | undefined | PossiblyNewIssue = isCreatingNewIssue
    ? defaultIssue
    : storeIssue ?? (fullIssue === null ? defaultIssue : fullIssue)
  const issueRef = useCurrentValue(issue)
  const autofocusRef = useAutofocus([issueId])

  const {
    register,
    handleSubmit,
    errors,
    formState,
    reset,
    control,
    watch,
  } = useForm<IssueFormData>({})

  const [serverErrors, setServerErrors] = useState<Array<string> | undefined>()

  useEffect(() => {
    if (fullIssue && fullIssue.id !== issueId) {
      setFullIssue(null)
    }
  }, [issueId, fullIssue])

  const fetchFullIssue = React.useCallback(
    (options: { refetch?: boolean } = {}) => {
      if (issueId) {
        if (!options.refetch && issueRef.current?.id === issueId) {
          reset(getDefaultValues(issueRef.current))
        }
        redmineStore
          .getState()
          .api.fetchFullIssue(issueId)
          .then((issue) => {
            setFullIssue(issue)
            if (issue) {
              reset(getDefaultValues(issue))
            }
          })
      }
    },
    [redmineStore, issueId, issueRef, reset],
  )

  useEffect(fetchFullIssue, [fetchFullIssue])

  useEffect(() => {
    if (isCreatingNewIssue && issueRef.current) {
      reset(getDefaultValues(issueRef.current))
    }
  }, [isCreatingNewIssue, issueRef, reset])

  if (issue === undefined) {
    if (currentRawIssueId !== undefined) {
      setTimeout(() => {
        navigate("..", { replace: true })
      })
    }
    return null
  }

  return (
    <Dialog
      open={
        currentRawIssueId !== undefined &&
        (issue !== undefined || isCreatingNewIssue)
      }
      onClose={() => {
        if (!formState.isDirty) {
          navigate("..", { replace: true })
        }
      }}
    >
      {({ ref, style }) => (
        <form
          style={{
            ...style,

            position: "relative",
            display: "grid",
            gridTemplateColumns: "1fr 250px",
            gridTemplateRows: "64px auto 1fr",
            boxSizing: "border-box",
            padding: 16,
            gridColumnGap: 16,
            width: 1000,
            maxWidth: "calc(100vw - 32px)",
            minHeight: 410,
            maxHeight: "100%",
          }}
          ref={ref}
          onSubmit={handleSubmit(async (data) => {
            try {
              const newIssue = await saveData(data, issue, redmineStore)
              reset({ ...data, comment: "" })
              if (serverErrors) setServerErrors(undefined)
              if (newIssue) {
                navigate(`../${newIssue.id}`, { replace: true })
              } else {
                fetchFullIssue({ refetch: true })
              }
            } catch (error) {
              console.error("Error saving issue", error)
              let didSetNiceError = false
              if (error instanceof ApiError && error.response.status === 422) {
                const response = await error.response.json()
                if (response.errors?.length > 0) {
                  didSetNiceError = true
                  setServerErrors(response.errors)
                }
              }
              if (!didSetNiceError) {
                setServerErrors(["An error occured while saving"])
              }
            }
          })}
          onKeyDown={submitOnCtrlEnter}
        >
          <div
            style={{
              display: "grid",
              gridColumn: "span 2",
              gridTemplateColumns: "128px 1fr 128px",
              alignItems: "center",
            }}
          >
            <IconButton
              style={{
                alignSelf: "start",
                marginTop: -8,
                marginLeft: -8,
                fontSize: 32,
              }}
              onClick={() => navigate("..", { replace: true })}
            >
              <CloseIcon />
            </IconButton>
            <h2
              style={{
                display: "flex",
                justifyContent: "center",
                margin: 0,
                color: theme.palette.text.secondary,
                userSelect: "text",
              }}
            >
              {issue.id === undefined ? (
                isCreatingNewIssue ? (
                  <span>
                    New <TrackerName trackerId={+watch("tracker")} />
                  </span>
                ) : null
              ) : (
                <>
                  <span>
                    {issue.tracker.name} #{issueId}
                  </span>
                  <div style={{ position: "relative" }}>
                    <IconLinkButton
                      href={redmineStore.formatIssueUrl(issue.id)}
                      target="redmine"
                      title="Open in Redmine"
                      style={{
                        position: "absolute",
                        top: -10,
                        left: 4,
                        color: `rgba(255, 255, 255, 0.87)`,
                      }}
                    >
                      <ExternalLinkIcon />
                    </IconLinkButton>
                  </div>
                </>
              )}
            </h2>
            <Button
              style={{ justifySelf: "end", padding: "12px 24px" }}
              disabled={!formState.isDirty && !isCreatingNewIssue}
            >
              Save
            </Button>
          </div>
          <div style={{ gridColumn: "span 2" }}>
            {serverErrors?.map((error) => (
              <div
                style={{
                  color: theme.palette.text.error,
                  textAlign: "center",
                }}
              >
                {error}
              </div>
            ))}
          </div>
          <div
            ref={autofocusRef}
            style={{
              display: "grid",
              alignContent: "start",
              gridTemplateColumns: "100%",
              gridGap: 16,
              paddingRight: 8,
              overflowX: "hidden",
              overflowY: "auto",
              scrollbarColor: `${theme.palette.background.above} ${theme.palette.background.area}`,
              scrollbarWidth: "thin",
            }}
          >
            <Label>
              <span>Subject</span>
              <Textarea
                data-autofocus
                name="subject"
                ref={register({ required: true })}
                rows={1}
              />
              {errors.subject && <span>Subject is required</span>}
            </Label>
            <Description issue={issue} register={register} />
            <div>
              {issue.attachments.map((attachment) =>
                attachment.contentType.startsWith("image/") ? (
                  <a
                    key={attachment.id}
                    href={attachment.contentUrl}
                    target="attachment"
                  >
                    <img
                      alt={attachment.description}
                      title={attachment.filename}
                      src={attachment.thumbnailUrl}
                      style={{ borderRadius: 3 }}
                    />
                  </a>
                ) : (
                  <a
                    key={attachment.id}
                    href={attachment.contentUrl}
                    target="attachment"
                  >
                    {attachment.filename}
                  </a>
                ),
              )}
            </div>
            {issue.id !== undefined && (
              <Comments
                register={register}
                issue={issue}
                fullIssue={fullIssue}
              />
            )}
            {issue.id !== undefined && (
              <ChildIssues issue={issue} fullIssue={fullIssue} />
            )}
          </div>
          <div
            style={{
              display: "grid",
              alignContent: "start",
              gridGap: 16,
              overflow: "hidden",
            }}
          >
            <Label>
              <span>Tracker</span>
              <Select name="tracker" ref={register({ required: true })}>
                {trackers.map((tracker) => (
                  <option key={tracker.id} value={tracker.id}>
                    {tracker.name}
                  </option>
                ))}
                {errors.tracker && <span>Tracker is required</span>}
              </Select>
            </Label>
            <Label>
              <span>Priority</span>
              <Select name="priority" ref={register({ required: true })}>
                {priorities.map((priority) => (
                  <option key={priority.id} value={priority.id}>
                    {priority.name}
                  </option>
                ))}
                {errors.priority && <span>Priority is required</span>}
              </Select>
            </Label>
            <Label>
              <span>Target Version</span>
              <Select name="version" ref={register}>
                <option value="">none</option>
                {versions.map((version) => (
                  <option key={version.id} value={version.id}>
                    {version.name}
                  </option>
                ))}
              </Select>
            </Label>
            <Controller
              name="parent"
              defaultValue={null}
              render={({ ref, ...props }) => (
                <IssuePicker
                  {...props}
                  inputRef={ref}
                  label="Parent"
                  filterIssues={(issue.id ? [issue.id] : []).concat(
                    fullIssue?.children.map((i) => i.id) ?? [],
                  )}
                />
              )}
              control={control}
            />
            <Label>
              <span>Status</span>
              <Select name="status" ref={register({ required: true })}>
                {statuses.map((status) => (
                  <option key={status.id} value={status.id}>
                    {status.name}
                  </option>
                ))}
                {errors.status && <span>Status is required</span>}
              </Select>
            </Label>
            <Label>
              <span>Assignee</span>
              <Select name="assignee" ref={register}>
                <option value="">nobody</option>
                {members.map((user) => (
                  <option key={user.id} value={user.id}>
                    {user.name}
                  </option>
                ))}
              </Select>
            </Label>
            {!isCreatingNewIssue &&
              customMemberFields.map((field) => (
                <Label key={field}>
                  <span>{field}</span>
                  <Select name={`customFields.${field}`} ref={register}>
                    <option value="">nobody</option>
                    {members.map((user) => (
                      <option key={user.id} value={user.id}>
                        {user.name}
                      </option>
                    ))}
                  </Select>
                </Label>
              ))}
          </div>
        </form>
      )}
    </Dialog>
  )
}

const TrackerName = (props: { trackerId: number }) => {
  const trackerName = redmineContext.useStateSlice(
    (redmine) =>
      redmine.data?.trackers.find((t) => t.id === props.trackerId)?.name,
    { deps: [props.trackerId] },
  )

  return <>{trackerName}</>
}

const LazyMarkdown = React.lazy(() =>
  import(
    /* webpackChunkName: "Markdown" */ "@app/ui/Markdown"
  ).then((module) => ({ default: module.Markdown })),
)

const Description = (props: {
  issue?: PossiblyNewIssue
  register: () => void
}) => {
  const [showEditor, setShowEditor] = useState(true)

  useEffect(() => {
    setShowEditor(props.issue?.id == null || props.issue.description === "")
  }, [props.issue])

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center" }}>
        <span style={{ flex: 1 }}>Description</span>
        <IconButton
          onClick={() => setShowEditor((showEditor) => !showEditor)}
          title={showEditor ? "Show rendered description" : "Edit description"}
        >
          <EditIcon />
        </IconButton>
      </div>
      <Textarea
        name="description"
        ref={props.register}
        rows={10}
        style={showEditor ? { width: "100%" } : { display: "none" }}
      />
      {!showEditor && (
        <LazyMarkdown
          text={props.issue?.description ?? ""}
          style={{ userSelect: "text" }}
        />
      )}
    </div>
  )
}
