import { redmineContext } from "@app/stores/RedmineStore"
import { IconLinkButton, LinkButton } from "@app/styles"
import type { FullIssue, Issue } from "@app/types"
import AddIcon from "@app/ui/icons/Plus"
import * as React from "react"
import { useTheme } from "styled-components"
import { newIssue } from "./formManagement"

export const ChildIssues = (props: {
  issue: Issue
  fullIssue: FullIssue | undefined | null
}) => {
  const swimlaneTrackers = redmineContext.useStateSlice(
    s => s.settings.swimlaneTrackers,
  )

  if (
    props.fullIssue?.children.length === 0 &&
    !swimlaneTrackers.includes(props.issue.tracker.name)
  )
    return null

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center" }}>
        <h3 style={{ flex: 1, margin: 0 }}>Children</h3>
        <IconLinkButton
          to={`../${newIssue}?parent=${props.issue.id}`}
          style={{ margin: 0 }}
          title="Create subtask"
        >
          <AddIcon />
        </IconLinkButton>
      </div>
      {props.fullIssue?.children.map(issue => (
        <ChildIssue key={issue.id} issue={issue} />
      ))}
    </div>
  )
}

const ChildIssue = React.memo(({ issue }: { issue: Issue }) => {
  const theme = useTheme()
  const isClosed = redmineContext.useStateSlice(
    redmine =>
      redmine.data?.statuses.find(s => s.id === issue.status.id)?.isClosed,
    { deps: [issue.status.id] },
  )

  return (
    <LinkButton
      to={`../${issue.id}`}
      style={{
        marginTop: 8,
        padding: "4px 8px",
        color: isClosed
          ? theme.palette.text.secondary
          : theme.palette.text.main,
        opacity: isClosed ? 0.6 : 1,
      }}
    >
      <div
        style={{
          display: "flex",
          paddingBottom: 3,
          fontSize: "0.9em",
          color: theme.palette.text.secondary,
        }}
      >
        <span style={{ flex: 1 }}>
          #{issue.id} - {issue.tracker.name}
        </span>
        <span>{issue.version?.name}</span>
      </div>
      <div
        style={{
          display: "flex",
        }}
      >
        <span
          style={{
            flex: 1,
            paddingRight: 8,
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          }}
        >
          {issue.subject}
        </span>
        <span>{issue.status.name}</span>
      </div>
    </LinkButton>
  )
})
