import { IssueAction, boardContext } from "@app/stores/BoardStore"
import { projectContext } from "@app/stores/ProjectStore"
import { RedmineStore, redmineContext } from "@app/stores/RedmineStore"
import type { Clearable, CustomField, Issue, NewIssue } from "@app/types"
import { useMemo } from "react"

type NewIssueExtended = NewIssue & {
  id?: undefined
  customFields: Issue["customFields"]
  attachments: Issue["attachments"]
}
export type PossiblyNewIssue = Issue | NewIssueExtended

type FormValues<T> = {
  [K in keyof T]-?: string
}

export type IssueFormData = FormValues<
  Pick<
    Issue,
    | "subject"
    | "description"
    | "tracker"
    | "priority"
    | "status"
    | "assignee"
    | "version"
    | "parent"
  >
> & {
  comment?: string
  customFields?: Record<string, string>
}

const int = (value: string | undefined | null) =>
  value == null || value === "" ? undefined : +value

export const submitOnCtrlEnter = (
  e: React.KeyboardEvent<HTMLTextAreaElement | HTMLFormElement>,
) => {
  if (e.ctrlKey && e.key === "Enter") {
    const form =
      e.currentTarget instanceof HTMLFormElement
        ? e.currentTarget
        : e.currentTarget.form

    if (form) {
      e.stopPropagation()
      e.preventDefault()
      if (form.requestSubmit) {
        form.requestSubmit()
      } else {
        const submitEvent = document.createEvent("Event")
        submitEvent.initEvent("submit", true, true)
        form.dispatchEvent(submitEvent)
      }
    }
  }
}

export const getDefaultValues = (issue: PossiblyNewIssue): IssueFormData => ({
  subject: issue.subject,
  description: issue.description,
  tracker: issue.tracker.id.toString(),
  priority: issue.priority.id.toString(),
  status: issue.status.id.toString(),
  assignee: issue.assignee?.toString() ?? "",
  version: issue.version?.id.toString() ?? "",
  parent: issue.parent?.toString() ?? "",
  customFields: Object.fromEntries(
    issue.customFields.map((field) => [field.name, field.value ?? ""]),
  ),
  comment: "",
})

export const newIssue = "new-issue"

export const useDefaultIssue = (): NewIssueExtended => {
  const redmineStore = redmineContext.useStore()
  const projectStore = projectContext.useStore()
  const boardStore = boardContext.useStore()

  const trackers = redmineStore.getState().data?.trackers ?? []
  const priorities = redmineStore.getState().data?.priorities ?? []
  const statuses = redmineStore.getState().data?.statuses ?? []
  const projectId = projectStore.getState().id
  const version = boardStore.getState().version

  return useMemo(() => {
    const queryParams = new URLSearchParams(window.location.search)
    const parentRaw = queryParams.get("parent")
    let parent = parentRaw && !isNaN(+parentRaw) ? +parentRaw : undefined

    return {
      projectId,
      subject: "",
      description: "",
      tracker: trackers[0],
      priority: priorities.find((p) => p.isDefault) ?? priorities[0],
      status: statuses[0],
      version,
      parent,
      customFields: [],
      attachments: [],
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [projectId, statuses, trackers, version, window.location.search])
}

export async function saveData(
  data: IssueFormData,
  issue: PossiblyNewIssue,
  redmineStore: RedmineStore,
) {
  const redmineData = redmineStore.getState().data
  const projectStore = redmineStore.getState().projectStore
  const boardStore = projectStore?.getState().boardStore
  if (
    redmineData === undefined ||
    projectStore === undefined ||
    boardStore === undefined
  ) {
    return
  }

  const { customMemberFields } = redmineStore.getState().settings
  const { priorities, statuses } = redmineData
  const { members, trackers, versions } = projectStore.getState()

  const modifications: Array<IssueAction> = []

  if (int(data.status) !== issue.status.id) {
    modifications.push({
      type: "setStatus",
      status: statuses.find((s) => s.id === int(data.status))!,
      index: undefined,
    })
  }

  if (int(data.parent) !== issue.parent) {
    modifications.push({
      type: "setParentIssue",
      parent: int(data.parent),
      index: undefined,
    })
  }

  if (int(data.version) !== issue.version?.id) {
    modifications.push({
      type: "setVersion",
      version:
        data.version === ""
          ? undefined
          : versions.find((t) => t.id === int(data.version)),
    })
  }

  {
    const fields: Partial<
      Clearable<Omit<Issue, "id" | "status" | "parent">>
    > = {}

    if (data.subject !== issue.subject) {
      fields.subject = data.subject
    }
    if (data.description !== issue.description) {
      fields.description = data.description
    }
    if (int(data.tracker) !== issue.tracker.id) {
      fields.tracker = trackers.find((t) => t.id === int(data.tracker))
    }
    if (int(data.priority) !== issue.priority.id) {
      fields.priority = priorities.find((t) => t.id === int(data.priority))
    }
    if (int(data.assignee) !== issue.assignee) {
      fields.assignee =
        data.assignee === "" ? null : members.get(int(data.assignee)!)?.id
    }

    if (Object.keys(fields).length > 0) {
      modifications.push({
        type: "setFields",
        fields,
      })
    }
  }

  if (issue.id !== undefined) {
    const fields: Array<Pick<CustomField, "name" | "value">> = []

    for (const field of customMemberFields) {
      const currentValue =
        issue.customFields.find((f) => f.name === field)?.value ?? null
      const formValue = data.customFields?.[field] ?? null

      if (formValue !== currentValue) {
        fields.push({ name: field, value: formValue })
      }
    }

    if (fields.length > 0) {
      modifications.push({
        type: "setCustomFields",
        fields,
      })
    }
  }

  if (modifications.length > 0 || data.comment) {
    if (issue.id === undefined) {
      const newIssue = { ...issue }
      modifications.forEach((modification) => {
        switch (modification.type) {
          case "setStatus": {
            newIssue.status = modification.status
            break
          }
          case "setParentIssue": {
            newIssue.parent = modification.parent
            break
          }
          case "setVersion": {
            newIssue.version = modification.version
            break
          }
          case "setFields": {
            Object.assign(newIssue, modification.fields)
            break
          }
          case "setCustomFields": {
            // TODO: Support creating custom fields
          }
        }
      })
      return await boardStore.createIssue(newIssue)
    } else {
      await boardStore.updateIssue({
        issueId: issue.id,
        comment: data.comment || undefined,
        modifications,
      })
    }
  }
}
