import { boardContext } from "@app/stores/BoardStore"
import { redmineContext } from "@app/stores/RedmineStore"
import { Textarea } from "@app/styles"
import type {
  ChangedPropertyKey,
  ChangedPropertyValue,
  FullIssue,
  HistoryEvent,
  Issue,
} from "@app/types"
import * as React from "react"
import { Link } from "react-router-dom"
import { useTheme } from "styled-components"

const LazyMarkdown = React.lazy(() =>
  import(
    /* webpackChunkName: "Markdown" */ "@app/ui/Markdown"
  ).then(module => ({ default: module.Markdown })),
)

export const Comments = (props: {
  issue: Issue
  fullIssue: FullIssue | undefined | null
  register: (e: HTMLTextAreaElement | null) => void
}) => {
  const createdIssueEvent = React.useMemo(
    (): HistoryEvent => ({
      id: 0,
      notes: "",
      createdOn: props.issue.createdOn,
      user: props.issue.createdBy,
      changes: [{ type: "createdIssue" }],
    }),
    [props.issue.createdOn, props.issue.createdBy],
  )

  return (
    <React.Suspense fallback={null}>
      <div>
        <h3 style={{ margin: 0 }}>History</h3>
        <HistoryEventView event={createdIssueEvent} />
        {props.fullIssue?.history.map(event => (
          <HistoryEventView key={event.id} event={event} />
        ))}
        <Textarea
          name="comment"
          ref={props.register}
          rows={2}
          async
          style={{ marginTop: 16, width: "100%" }}
          placeholder="New comment"
        />
      </div>
    </React.Suspense>
  )
}

const formatDate = (date: Date) =>
  `${date
    .getFullYear()
    .toString()
    .padStart(4, "0")}-${(date.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${date
    .getDate()
    .toString()
    .padStart(2, "0")} ${date
    .getHours()
    .toString()
    .padStart(2, "0")}:${date
    .getMinutes()
    .toString()
    .padStart(2, "0")}`

const propertyNames: { [P in ChangedPropertyKey]: string } = {
  subject: "subject",
  description: "description",
  projectId: "project",
  tracker: "tracker",
  priority: "priority",
  status: "status",
  parent: "parent issue",
  version: "version",
  assignee: "assignee",
}

const HistoryEventView = React.memo(({ event }: { event: HistoryEvent }) => {
  const theme = useTheme()

  return (
    <div style={{ marginTop: 16, userSelect: "text" }}>
      <div
        style={{
          display: "flex",
          color: theme.palette.text.secondary,
          borderBottom: `thin solid ${theme.palette.divider.weak}`,
        }}
      >
        <span style={{ flex: 1 }}>{event.user.name}</span>
        {formatDate(event.createdOn)}
      </div>
      {event.changes.length > 0 && (
        <div style={{ paddingTop: 4 }}>
          {event.changes.map((change, i) => (
            <div
              key={i}
              style={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                color: "#b0b0b0",
              }}
            >
              {change.type === "createdIssue" ? (
                <>Created issue</>
              ) : change.type === "property" ? (
                change.action === "removed" ? (
                  <>
                    Removed {propertyNames[change.property]}:{" "}
                    <PropertyValue
                      property={change.property}
                      value={change.oldValue}
                    />
                  </>
                ) : change.action === "added" ? (
                  <>
                    Set {propertyNames[change.property]}:{" "}
                    <PropertyValue
                      property={change.property}
                      value={change.newValue}
                    />
                  </>
                ) : (
                  <>
                    Changed {propertyNames[change.property]}:{" "}
                    <PropertyValue
                      property={change.property}
                      value={change.oldValue}
                    />{" "}
                    →{" "}
                    <PropertyValue
                      property={change.property}
                      value={change.newValue}
                    />
                  </>
                )
              ) : (
                <>
                  {change.action === "added" ? "Added" : "Removed"} attachment{" "}
                  {change.filename}
                </>
              )}
            </div>
          ))}
        </div>
      )}
      {event.notes.length > 0 && (
        <LazyMarkdown text={event.notes} style={{ margin: "8px 4px" }} />
      )}
    </div>
  )
})

function PropertyValue<P extends ChangedPropertyValue>(props: P) {
  if (props.property === "subject" || props.property === "description") {
    return (
      <span
        style={{
          display: "inline-block",
          maxWidth: 250,
          overflow: "hidden",
          textOverflow: "ellipsis",
          whiteSpace: "nowrap",
          verticalAlign: "text-bottom",
        }}
        title={props.value as string}
      >
        {props.value}
      </span>
    )
  }

  if (props.property === "assignee") {
    return <Username id={props.value as number} />
  }
  if (props.property === "parent") {
    return <IssueLink id={props.value as number} />
  }

  const text: string =
    typeof props.value === "object"
      ? props.value.name
      : typeof props.value === "string"
      ? props.value
      : `${props.property}: ${props.value}`
  return <>{text}</>
}

function Username(props: { id: number }) {
  const user = redmineContext.useStateSlice(s => s.api.getUser(props.id), {
    deps: [props.id],
  })

  return user ? <>{user.name}</> : <>User {props.id}</>
}

function IssueLink(props: { id: number }) {
  const issue = boardContext.useStateSlice(s => s.issues.get(props.id), {
    deps: [props.id],
  })

  return (
    <Link
      to={`../${props.id}`}
      title={issue ? `${issue.tracker.name}: ${issue.subject}` : undefined}
    >
      #{props.id}
    </Link>
  )
}
