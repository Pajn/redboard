import { ApiError } from "@app/api/api"
import { redmineContext } from "@app/stores/RedmineStore"
import { Button, Input, Label, centerContent } from "@app/styles"
import * as React from "react"
import { useEffect, useState } from "react"

export const SignInPage = () => {
  const redmineStore = redmineContext.useStore()
  const apiSettings = redmineContext.useStateSlice(api => api.settings)

  const [baseUrl, setBaseUrl] = useState(apiSettings.baseUrl ?? "")
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [error, setError] = useState("")

  useEffect(() => {
    if (apiSettings.baseUrl) {
      setBaseUrl(apiSettings.baseUrl)
    }
  }, [apiSettings.baseUrl])

  return (
    <div style={centerContent}>
      <form
        style={{
          display: "grid",
          gridGap: 16,
          marginTop: 200,
          padding: 24,
          width: 300,
          borderRadius: 5,
          backgroundColor: "hsl(243, 10%, 25%)",
        }}
        onSubmit={e => {
          e.preventDefault()
          const form = e.currentTarget

          redmineStore.setBaseUrl(baseUrl)
          redmineStore
            .getState()
            .api.login(username, password)
            .then(user => {
              redmineStore.setApiKey(user.apiKey)
            })
            .catch(err => {
              console.error("Login error", err, form)
              if (err instanceof ApiError && err.response.status === 401) {
                setError("Invalid username or password")
              } else {
                setError("Invalid Redmine URL")
              }

              if (form.animate) {
                form.animate(
                  [
                    { transform: "translateX(0)" },
                    { transform: "translateX(-10px)" },
                    { transform: "translateX(10px)" },
                    { transform: "translateX(-10px)" },
                    { transform: "translateX(10px)" },
                    { transform: "translateX(0)" },
                  ],
                  { duration: 400 },
                )
              }
            })
        }}
      >
        <Label>
          <span>Redmine URL</span>
          <Input
            type="url"
            value={baseUrl}
            onChange={e => setBaseUrl(e.target.value)}
          />
        </Label>
        <Label>
          <span>Username</span>
          <Input value={username} onChange={e => setUsername(e.target.value)} />
        </Label>
        <Label>
          <span>Password</span>
          <Input
            type="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </Label>
        <div
          style={{
            ...centerContent,
            height: 42,
            color: "hsl(359, 90%, 55%)",
            opacity: error ? 1 : 0,
            transition: "opacity 200ms ease",
          }}
        >
          {error}
        </div>
        <Button style={{ height: 42 }}>Login</Button>
      </form>
    </div>
  )
}
