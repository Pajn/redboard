import { boardContext } from "@app/stores/BoardStore"
import { projectContext } from "@app/stores/ProjectStore"
import { centerContent } from "@app/styles"
import type { Issue } from "@app/types"
import { usePageVisibility } from "@app/ui/usePageVisibility"
import { Droppable } from "@pajn/react-beautiful-dnd"
import React, { CSSProperties, Fragment, ReactNode } from "react"
import { Link } from "react-router-dom"
import styled, { useTheme } from "styled-components"
import { IssueCard } from "./IssueCard"

export const BoardView = () => {
  const theme = useTheme()
  const columns = projectContext.useStateSlice(project => project.columns)
  const structure = boardContext.useStateSlice(board => board.structure)
  const boardStore = boardContext.useStore()

  usePageVisibility(() => {
    if (!document.hidden) {
      const { fetchedAt } = boardStore.getState()
      if (Date.now() - fetchedAt.getTime() > 10_000) {
        boardStore.refresh()
      }
    }
  })

  return (
    <div
      style={{
        display: "grid",
        gridTemplateColumns: `repeat(${columns.length}, 1fr)`,
        alignContent: "start",
        borderRadius: 3,
        backgroundColor: theme.board.background,
      }}
    >
      {columns.map(column => (
        <ColumnHeader key={column.name} column={column.name} />
      ))}
      {structure.map(([swimlane, swimlaneColumns]) => (
        <Fragment key={swimlane.id}>
          <Swimlane
            swimlane={swimlane}
            style={{ gridColumn: `span ${columns.length}` }}
          />
          {columns.map(column => (
            <IssueList
              key={column.name}
              swimlane={swimlane}
              column={column.name}
            >
              {swimlaneColumns.get(column.name)?.map((issueId, issueIndex) => (
                <IssueCard key={issueId} issueId={issueId} index={issueIndex} />
              ))}
            </IssueList>
          ))}
        </Fragment>
      ))}
    </div>
  )
}

const ColumnHeader = (props: { column: string }) => {
  const theme = useTheme()

  return (
    <div
      style={{
        ...centerContent,
        position: "sticky",
        top: 0,
        marginBottom: 4,
        padding: 8,
        zIndex: 2,
        borderBottom: `thin solid ${theme.board.headerDivider}`,
        backgroundColor: theme.board.background,
      }}
    >
      {props.column}
    </div>
  )
}

const Swimlane = (props: { swimlane: Issue; style?: CSSProperties }) => {
  const theme = useTheme()

  return (
    <div
      style={{
        position: "sticky",
        top: 32,
        zIndex: 1,
        paddingTop: 8,
        paddingLeft: 8,
        paddingBottom: 3,
        borderBottom: `thin solid ${theme.board.swimlaneDivider}`,
        backgroundColor: theme.board.background,
        ...props.style,
      }}
    >
      <SwimlaneLink to={`${props.swimlane.id}`}>
        #{props.swimlane.id} - {props.swimlane.tracker.name}:{" "}
        {props.swimlane.subject}
      </SwimlaneLink>
    </div>
  )
}

const SwimlaneLink = styled(Link)({
  textDecorationColor: "transparent",
  color: "inherit",
  outline: "none",

  transition: "text-decoration-color 200ms ease",

  "&:hover, &.focus-visible": {
    textDecorationColor: "currentColor",
  },
})

const IssueList = (props: {
  swimlane: Issue
  column: string
  children: ReactNode
}) => {
  return (
    <Droppable droppableId={`${props.swimlane.id}-${props.column}`}>
      {provided => (
        <IssueListContainer
          ref={provided.innerRef}
          {...provided.droppableProps}
        >
          {props.children}
          {provided.placeholder}
        </IssueListContainer>
      )}
    </Droppable>
  )
}
const IssueListContainer = styled.div({
  display: "flex",
  flexDirection: "column",
  marginLeft: 2,
  marginRight: 2,
  padding: 4,
  paddingBottom: 20,
  overflow: "hidden",
  minHeight: 48,
})
