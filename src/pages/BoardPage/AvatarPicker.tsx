import { projectContext } from "@app/stores/ProjectStore"
import { arrowUp } from "@app/styles"
import type { User } from "@app/types"
import { Avatar, AvatarProps } from "@app/ui/Avatar"
import { Popper } from "@app/ui/Popper"
import {
  EnterLeaveTransitionController,
  useEnterLeaveTransition,
} from "@app/ui/transitions/useEnterLeaveTransition"
import type { AnimatableElement } from "@app/ui/transitions/utils"
import { elementHasFocus } from "@app/ui/utils"
import offset from "@popperjs/core/lib/modifiers/offset"
import * as React from "react"
import { useRef, useState } from "react"

export const AvatarPicker = ({
  userId,
  size = 32,
  onPick,
  forceVisible: forceVisibleProp,
  ...props
}: Omit<AvatarProps, "user" | "hidden" | "button" | "description"> & {
  description: string
  userId?: number
  onPick: (user?: User) => void
  forceVisible?: boolean
}) => {
  const avatarRef = useRef<HTMLElement | null>(null)
  const popperRef = useRef<HTMLElement | null>(null)
  const [open, setOpen] = useState(false)
  const members = projectContext.useStateSlice(project => project.members)
  const [membersToShow, setMembersToShow] = useState<Array<User | undefined>>(
    [],
  )
  const user = userId != null ? members.get(userId) : undefined

  const hasFocus = () =>
    elementHasFocus(avatarRef.current) || elementHasFocus(popperRef.current)

  const membersTransition = useEnterLeaveTransition({ in: open }, reveal)

  return (
    <Avatar
      {...props}
      user={user}
      hidden={userId == null && !forceVisibleProp && !membersTransition.mount}
      button
      size={size}
      ref={avatarRef}
      onClick={e => {
        e.stopPropagation()
        setOpen(!open)
        if (!open) {
          const membersToShow: Array<User | undefined> = [...members.values()]
            .filter(user => user.id !== userId)
            .sort((a, b) => a.name.localeCompare(b.name))
          if (userId != null) membersToShow.push(undefined)
          setMembersToShow(membersToShow)
        }
      }}
      onBlur={(e) => {
        if (!hasFocus() && (!e.relatedTarget || !popperRef.current?.contains(e.relatedTarget as Node))) {
          setOpen(false)
        }
      }}
    >
      <Popper
        ref={popperRef}
        open={membersTransition.in || open}
        anchorEl={avatarRef.current}
        modifiers={[
          {
            ...offset,
            options: {
              offset: [0, 6],
            },
          },
        ]}
        onBlur={() => {
          if (!hasFocus()) {
            setOpen(false)
          }
        }}
        style={{
          zIndex: 10,
        }}
      >
        <div
          ref={membersTransition.ref}
          style={{
            position: "relative",
            display: "flex",
            justifyContent: "center",
            zIndex: 1,
          }}
        >
          <div
            style={{
              ...arrowUp(5, "rgba(0, 0, 0, 0.7)"),
              position: "absolute",
              top: -5,
            }}
          />
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              width: "max-content",
              maxWidth: (32 + 6) * 5,
              padding: 6,
              borderRadius: 6,
              backgroundColor: "rgba(0, 0, 0, 0.7)",
            }}
          >
            Set {props.description}
            <div
              style={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "center",
                paddingTop: 8,
                width: "max-content",
                maxWidth: "100%",
              }}
            >
              {membersToShow.map(user => (
                <Avatar
                  key={user?.id ?? -1}
                  button
                  user={user}
                  size={32}
                  style={{ margin: 3 }}
                  onClick={e => {
                    e.stopPropagation()
                    onPick(user)
                    setOpen(false)
                  }}
                />
              ))}
            </div>
          </div>
        </div>
      </Popper>
    </Avatar>
  )
}

function reveal(
  element: AnimatableElement,
  transition: Readonly<EnterLeaveTransitionController>,
) {
  element.style.transition = transition.isAnimating
    ? `clip-path ${transition.in ? 200 : 300}ms ease-in-out`
    : ""
  element.style.clipPath = transition.isFinished
    ? ""
    : transition.in
    ? "circle(110% at top center)"
    : "circle(0 at top center)"
}
