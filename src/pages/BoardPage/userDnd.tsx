import { boardContext } from "@app/stores/BoardStore"
import { projectContext } from "@app/stores/ProjectStore"
import { redmineContext } from "@app/stores/RedmineStore"
import { centerContent } from "@app/styles"
import { User, assertType } from "@app/types"
import { Avatar, AvatarProps } from "@app/ui/Avatar"
import {
  EnterLeaveTransitionController,
  useEnterLeaveTransition,
} from "@app/ui/transitions/useEnterLeaveTransition"
import type { AnimatableElement } from "@app/ui/transitions/utils"
import React, { useEffect, useRef } from "react"
import { XYCoord, useDrag, useDragLayer, useDrop } from "react-dnd"
import { getEmptyImage } from "react-dnd-html5-backend"
import { useTheme } from "styled-components"

type Range<T = number> = [T, T]
const lerp = (from: Range, to: Range) => {
  const fromOffset = from[0]
  const fromScale = from[1] - fromOffset
  const toOffset = to[0]
  const toScale = to[1] - toOffset

  return (value: number) => {
    const cappedValue = Math.min(from[1], Math.max(fromOffset, value))
    const percentage = (cappedValue - fromOffset) / fromScale
    return percentage * toScale + toOffset
  }
}

const distanceToDuration = lerp([0, 1500], [150, 330])

const userType = "user"
export type DragItem = { type: typeof userType; id: number; size?: number }

export const DraggableAvatar = (
  props: Omit<AvatarProps, "user"> & { user: User },
) => {
  const ref = useRef<HTMLElement | null>(null)
  const [{ isDragging }, drag, preview] = useDrag({
    item: assertType<DragItem>({
      type: userType,
      id: props.user.id,
      size: props.size,
    }),
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
    end(_dropResult, monitor) {
      if (ref.current && ref.current.animate) {
        if (monitor.didDrop()) {
          ref.current.animate(
            [{ transform: "scale(0)" }, { transform: "scale(1)" }],
            { duration: 150, easing: "ease-out" },
          )
        } else {
          const diff = monitor.getDifferenceFromInitialOffset()
          if (diff) {
            const distance = Math.sqrt(
              Math.abs(diff.x) ** 2 + Math.abs(diff.y) ** 2,
            )
            const duration = distanceToDuration(distance)
            ref.current.animate(
              [
                { transform: `translate(${diff.x}px, ${diff.y}px)` },
                { transform: "translate(0, 0)" },
              ],
              { duration, easing: "cubic-bezier(.2,1,.1,1)" },
            )
          }
        }
      }
    },
  })

  useEffect(() => {
    preview(getEmptyImage())
  }, [preview])

  return (
    <Avatar
      ref={e => {
        drag(e)
        ref.current = e
      }}
      {...props}
      style={{
        opacity: isDragging ? 0 : 1,
        transition: "none",
      }}
    />
  )
}

export const useUserDrop = () =>
  useDrop({
    accept: userType,
    collect(monitor) {
      return {
        isDraggingUser: monitor.isOver() && monitor.getItemType() === userType,
      }
    },
  })

function fade(
  element: AnimatableElement,
  controller: Readonly<EnterLeaveTransitionController>,
) {
  element.style.transition = controller.isAnimating ? "opacity 200ms ease" : ""
  element.style.opacity = controller.in ? "1" : "0"
}

export const UserDropShield = (props: {
  isDraggingUser: boolean
  issueId: number
}) => {
  const theme = useTheme()
  const customMemberFields = redmineContext.useStateSlice(
    s => s.settings.customMemberFields,
  )
  const transition = useEnterLeaveTransition({ in: props.isDraggingUser }, fade)

  return transition.mount ? (
    <div
      ref={transition.ref}
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: "flex",
        backgroundColor: theme.board.card,
      }}
    >
      <UserDropShieldItem
        type="assignee"
        name="Assignee"
        issueId={props.issueId}
      />
      {customMemberFields.map(field => (
        <UserDropShieldItem
          key={field}
          type="custom"
          name={field}
          issueId={props.issueId}
        />
      ))}
    </div>
  ) : null
}

const UserDropShieldItem = (props: {
  type: "assignee" | "custom"
  name: string
  issueId: number
}) => {
  const theme = useTheme()
  const boardStore = boardContext.useStore()
  const [{ userIsOver }, drop] = useDrop({
    accept: userType,
    drop(item: DragItem, _monitor) {
      if (props.type === "assignee") {
        boardStore.updateIssue({
          issueId: props.issueId,
          modifications: [{ type: "setFields", fields: { assignee: item.id } }],
        })
      } else if (props.type === "custom") {
        boardStore.updateIssue({
          issueId: props.issueId,
          modifications: [
            {
              type: "setCustomFields",
              fields: [{ name: props.name, value: item.id.toString() }],
            },
          ],
        })
      }
    },
    collect(monitor) {
      return {
        userIsOver: monitor.isOver() && monitor.getItemType() === userType,
      }
    },
  })

  return (
    <div
      ref={drop}
      style={{
        ...centerContent,
        flex: 1,
        backgroundColor: userIsOver
          ? theme.palette.background.focus
          : "transparent",
      }}
    >
      <div>{props.name}</div>
    </div>
  )
}

function getItemStyles(
  initialOffset: XYCoord | null,
  currentOffset: XYCoord | null,
) {
  if (!initialOffset || !currentOffset) {
    return {
      display: "none",
    }
  }

  let { x, y } = currentOffset

  const transform = `translate(${x}px, ${y}px)`
  return {
    transform,
  }
}

export const UserDragLayer = () => {
  const { isDragging, item, initialOffset, currentOffset } = useDragLayer(
    monitor => ({
      item: monitor.getItem(),
      itemType: monitor.getItemType(),
      initialOffset: monitor.getInitialSourceClientOffset(),
      currentOffset: monitor.getSourceClientOffset(),
      isDragging: monitor.isDragging(),
    }),
  )

  const user = projectContext.useStateSlice(
    s => item && s.members.get(item.id),
    {
      deps: [item?.id, item?.type],
    },
  )

  if (!isDragging || !user) {
    return null
  }

  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        zIndex: 100,
        pointerEvents: "none",
      }}
    >
      <div style={getItemStyles(initialOffset, currentOffset)}>
        <Avatar user={user} size={item?.size} />
      </div>
    </div>
  )
}
