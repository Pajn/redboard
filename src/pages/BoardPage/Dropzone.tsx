import React, { ReactNode, useCallback, useRef, useState } from "react"
import { useDrag, useDrop } from "react-dnd"
import { centerContent } from "@app/styles"
import { assertType } from "@app/types"
import { requestNextFrame } from "@app/ui/transitions/utils"

const issueType = "issue"
type DragItem = { type: typeof issueType; id: number }
type DropResult = {
  dropClientRect: DOMRect
  onTransitionEnd: () => void
} | null

export const Dropzone = (props: {
  children: ReactNode
  onDrop: (issueId: number) => void
}) => {
  const ref = useRef<HTMLElement | null>(null)
  const [{ isOver }, drop] = useDrop({
    accept: issueType,
    drop(item: DragItem): DropResult {
      return (
        ref.current && {
          dropClientRect: ref.current.getBoundingClientRect(),
          onTransitionEnd: () => {
            props.onDrop(item.id)
          },
        }
      )
    },
    collect(monitor) {
      return {
        isOver: monitor.isOver() && monitor.getItemType() === issueType,
      }
    },
  })

  const setRef = useCallback(
    (e: HTMLElement | null) => {
      ref.current = e
      drop(e)
    },
    [drop],
  )

  return (
    <div
      ref={setRef}
      style={{
        ...centerContent,
        height: 72,
        borderRadius: 12,
        backgroundColor: `rgba(255, 255, 255, ${isOver ? 0.2 : 0.1})`,
        transition: "background-color 300ms ease",
      }}
    >
      {props.children}
    </div>
  )
}
export function useDropzoneDraggable(issueId: number) {
  const ref = useRef<HTMLElement | null>(null)
  const [haveDropped, setHaveDropped] = useState(false)

  const [{ didDropLast, isDraggingOver }, drag] = useDrag({
    item: assertType<DragItem>({
      type: issueType,
      id: issueId,
    }),
    end(_, monitor) {
      const result: DropResult = monitor.getDropResult()
      if (monitor.didDrop() && result) {
        setHaveDropped(true)

        const element = ref.current
        if (element) {
          const dragClientRect = element.getBoundingClientRect()
          element.style.transition = "none"
          element.style.transform = "none"

          // Animate towards the center of the dropzone
          requestAnimationFrame(() => {
            const dropCenterY =
              result.dropClientRect.top + result.dropClientRect.height / 2
            const dropCenterX =
              result.dropClientRect.left + result.dropClientRect.width / 2

            element.style.transition = "none"
            element.style.position = "fixed"
            element.style.top = `${dropCenterY - dragClientRect.height / 2}px`
            element.style.left = `${dropCenterX - dragClientRect.width / 2}px`
            element.style.transform = `translate(${dragClientRect.x +
              dragClientRect.width / 2 -
              dropCenterX}px, ${dragClientRect.y +
              dragClientRect.height / 2 -
              dropCenterY}px) scale(1)`

            requestNextFrame(() => {
              element.style.transition = "transform 0.4s, opacity 0.4s"
              element.style.transformOrigin = "center center"
              element.style.transform = "scale(0.1)"
              element.style.opacity = "0"

              const listener = (e: TransitionEvent) => {
                if (
                  e.target === e.currentTarget &&
                  e.propertyName === "transform"
                ) {
                  result.onTransitionEnd()
                  element.removeEventListener("transitionend", listener)
                }
              }
              element.addEventListener("transitionend", listener)
            })
          })
        }
      }
    },
    collect(monitor) {
      return {
        didDropLast: monitor.didDrop(),
        isDraggingOver:
          monitor.isDragging() && monitor.getTargetIds().length > 0,
      }
    },
  })

  const setRef = useCallback(
    (e: HTMLElement | null) => {
      ref.current = e
      drag(e)
    },
    [drag],
  )

  return [
    { didDrop: didDropLast || haveDropped, isDraggingOver },
    setRef,
  ] as const
}
