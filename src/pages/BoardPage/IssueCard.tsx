import { boardContext } from "@app/stores/BoardStore"
import { redmineContext } from "@app/stores/RedmineStore"
import { cssJoin } from "@app/styles"
import type { Issue } from "@app/types"
import DescriptionIcon from "@app/ui/icons/MenuAlt2"
import AttachmentsIcon from "@app/ui/icons/PaperClip"
import { translate } from "@app/ui/transitions/cssTransitions"
import {
  EnterLeaveTransition,
  FlipTransition,
} from "@app/ui/transitions/renderProps"
import type { EnterLeaveTransitionController } from "@app/ui/transitions/useEnterLeaveTransition"
import { AnimatableElement, useIsFirstFrame } from "@app/ui/transitions/utils"
import { Draggable } from "@pajn/react-beautiful-dnd"
import React, { CSSProperties, useState } from "react"
import { useNavigate } from "react-router-dom"
import { useTheme } from "styled-components"
import { AvatarPicker } from "./AvatarPicker"
import { useDropzoneDraggable } from "./Dropzone"
import { UserDropShield, useUserDrop } from "./userDnd"

const isFirefox = navigator.userAgent.includes("Firefox")

export const IssueCard = (props: { issueId: number; index: number }) => {
  const theme = useTheme()
  const navigate = useNavigate()
  const issue = boardContext.useStateSlice(
    board => board.issues.get(props.issueId),
    { deps: [props.issueId] },
  )
  const [{ isDraggingUser }, userDrop] = useUserDrop()
  const [dropzoneProps, dropzoneDrag] = useDropzoneDraggable(props.issueId)
  const peopleWidth = usePeopleAreaWidth(issue)

  if (!issue) return null

  return (
    <Draggable draggableId={`issue-${issue.id}`} index={props.index}>
      {(provided, snapshot) => (
        <div
          ref={e => {
            provided.innerRef(e)
            dropzoneDrag(e)
          }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          onClick={() => navigate(`${issue.id}`)}
          style={
            dropzoneProps.didDrop
              ? { zIndex: 1000 }
              : provided.draggableProps.style
          }
        >
          <div
            ref={userDrop}
            style={{
              position: "relative",
              display: "grid",
              gridTemplateRows: "1fr auto",
              gridColumnGap: 4,
              gridRowGap: 2,
              marginTop: 4,
              paddingTop: 4,
              paddingLeft: 8,
              paddingRight: 4,
              paddingBottom: 5,
              overflow: "hidden",
              borderRadius: 3,
              backgroundColor: theme.board.card,
              boxShadow:
                snapshot.isDragging && !snapshot.isDropAnimating
                  ? "0px 1px 3px 3px rgba(0,0,0,0.1)"
                  : undefined,
              opacity: dropzoneProps.isDraggingOver ? 0.7 : 1,
              transform: dropzoneProps.isDraggingOver
                ? `scale(0.9)`
                : undefined,
              transition: cssJoin(
                `transform 400ms ease`,
                snapshot.isDragging &&
                  (snapshot.isDropAnimating
                    ? "box-shadow 400ms ease"
                    : "box-shadow 100ms ease"),
              ),
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                paddingRight: peopleWidth ? peopleWidth + 8 : undefined,
                paddingBottom: 3,
                fontSize: "0.9em",
                color: theme.palette.text.secondary,
              }}
            >
              <span style={{ flex: 1 }}>
                #{issue.id} - {issue.tracker.name}
              </span>
              <FlipTransition
                effect={translate({ duration: 200 })}
                deps={[issue.assignee, peopleWidth, issue.attachments]}
              >
                <div
                  style={{
                    display: "grid",
                    gridAutoFlow: "column",
                    gridTemplateColumns: "1fr",
                    justifyContent: "start",
                    alignItems: "center",
                    gridGap: 4,
                    cursor: "default",
                    willChange: isFirefox ? "transform" : undefined,
                  }}
                >
                  {issue.description.length > 0 && (
                    <DescriptionIcon title="Issue has a description" />
                  )}
                  {issue.attachments.length > 0 && (
                    <AttachmentsIcon title="Issue contains attachments" />
                  )}
                  <PriorityBadge issue={issue} />
                </div>
              </FlipTransition>
            </div>
            <p style={{ margin: 0 }}>
              {/* Filler to avoid text going under the avatars */}
              <span
                style={{
                  float: "right",
                  width: peopleWidth,
                  height: 12,
                }}
              />
              {issue.subject}
            </p>
            <People
              issue={issue}
              style={{
                position: "absolute",
                top: 6,
                right: 6,
              }}
            />
            <UserDropShield
              isDraggingUser={isDraggingUser}
              issueId={issue.id}
            />
          </div>
        </div>
      )}
    </Draggable>
  )
}

const lowPriorityColors = ["#009900", "#007ae6", "#612f93"]
const highPriorityColors = ["#d30000", "#d9470e", "#cc6203"]

const PriorityBadge = (props: { issue: Issue }) => {
  const defaultPriorityIndex = redmineContext.useStateSlice(
    s => s.data?.priorities.findIndex(p => p.isDefault) ?? -1,
  )
  const priorityIndex = redmineContext.useStateSlice(
    s =>
      s.data?.priorities.findIndex(p => p.id === props.issue.priority.id) ?? -1,
    { deps: [props.issue.priority] },
  )
  const priority = redmineContext.useStateSlice(
    s => s.data?.priorities.find(p => p.id === props.issue.priority.id),
    { deps: [props.issue.priority] },
  )

  if (
    !priority ||
    priority.isDefault ||
    defaultPriorityIndex < 0 ||
    priorityIndex < 0
  )
    return null

  const indexDiff = priorityIndex - defaultPriorityIndex
  const color =
    indexDiff < 0
      ? lowPriorityColors[
          Math.min(Math.abs(indexDiff), lowPriorityColors.length) - 1
        ]
      : highPriorityColors[Math.min(indexDiff, highPriorityColors.length) - 1]

  return (
    <span
      style={{
        display: "inline-block",
        padding: "0.1em 4px",
        fontSize: "0.8em",
        fontWeight: 500,
        borderRadius: 3,
        color: "white",
        backgroundColor: color,
      }}
    >
      {priority.name}
    </span>
  )
}

const avatarSize = 32

const usePeopleAreaWidth = (issue: Issue | undefined) => {
  const customMemberFields = redmineContext.useStateSlice(
    s => s.settings.customMemberFields,
  )
  const customMemberFieldsWidth = customMemberFields.reduce(
    (totalWidth, name) => {
      const field = issue?.customFields.find(field => field.name === name)
      return field?.value ? (totalWidth || avatarSize) + 18 : totalWidth
    },
    0,
  )

  return customMemberFieldsWidth || (issue?.assignee ? avatarSize : 0)
}

const People = ({ issue, style }: { issue: Issue; style?: CSSProperties }) => {
  const isFirstFrame = useIsFirstFrame()
  const customMemberFields = redmineContext.useStateSlice(
    s => s.settings.customMemberFields,
  )
  const boardStore = boardContext.useStore()

  const [hoverIndex, setHoverIndex] = useState<number | null>(null)

  let haveAdditionalAvatar = false
  const customMemberValues: Array<{
    name: string
    value?: number
    index: number
  }> = []
  {
    let index = 0
    for (const name of customMemberFields) {
      const field = issue.customFields.find(field => field.name === name)
      if (field?.value) {
        customMemberValues.push({
          name: field.name,
          value: +field.value,
          index,
        })
        index += 1
        haveAdditionalAvatar = true
      } else if (field) {
        customMemberValues.push({ name: field.name, index })
      }
    }
  }

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row-reverse",
        pointerEvents: "none",
        ...style,
      }}
    >
      <AvatarPicker
        size={avatarSize}
        userId={issue.assignee}
        description="Assignee"
        onPick={user => {
          setHoverIndex(currentIndex =>
            currentIndex === 0 ? null : currentIndex,
          )
          boardStore.updateIssue({
            issueId: issue.id,
            modifications: [
              {
                type: "setFields",
                fields: { assignee: user?.id ?? null },
              },
            ],
          })
        }}
        forceVisible={haveAdditionalAvatar}
        onMouseEnter={haveAdditionalAvatar ? () => setHoverIndex(0) : undefined}
        onMouseLeave={
          haveAdditionalAvatar
            ? () => {
                setHoverIndex(currentIndex =>
                  currentIndex === 0 ? null : currentIndex,
                )
              }
            : undefined
        }
        style={{ pointerEvents: "auto" }}
      />
      {customMemberValues.map(field => (
        <div
          key={field.name}
          style={{
            position: "absolute",
            transform: `translateX(-${avatarSize -
              14 +
              (avatarSize - 14) * field.index +
              (hoverIndex !== null && hoverIndex <= field.index ? 12 : 0)}px)`,
            transition: `transform 200ms ease`,
            willChange: "transform",
          }}
        >
          <EnterLeaveTransition
            controller={scaleUp}
            animateInitial={isFirstFrame}
            in={field.value !== undefined}
          >
            <div style={{ transformOrigin: "center center" }}>
              <AvatarPicker
                size={avatarSize}
                userId={field.value}
                description={field.name}
                onPick={user => {
                  setHoverIndex(currentIndex =>
                    currentIndex === field.index + 1 ? null : currentIndex,
                  )
                  boardStore.updateIssue({
                    issueId: issue.id,
                    modifications: [
                      {
                        type: "setCustomFields",
                        fields: [
                          {
                            name: field.name,
                            value: user?.id.toString() ?? null,
                          },
                        ],
                      },
                    ],
                  })
                }}
                forceVisible
                onMouseEnter={() => setHoverIndex(field.index + 1)}
                onMouseLeave={() => {
                  setHoverIndex(currentIndex =>
                    currentIndex === field.index + 1 ? null : currentIndex,
                  )
                }}
                style={{ pointerEvents: "auto" }}
              />
            </div>
          </EnterLeaveTransition>
        </div>
      ))}
    </div>
  )
}

function scaleUp(
  element: AnimatableElement,
  controller: Readonly<EnterLeaveTransitionController>,
) {
  element.style.transition = controller.isAnimating
    ? `transform 200ms ease`
    : ``
  element.style.transform = controller.in ? "scale(1)" : "scale(0.1)"
}
