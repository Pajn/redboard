import React, { CSSProperties, ReactNode } from "react"
import { useTheme } from "styled-components"
import { boardContext } from "@app/stores/BoardStore"
import { Button } from "@app/styles"

export const SidebarGroup = (props: {
  name: ReactNode
  children: ReactNode
  style?: CSSProperties
  innerProps?: React.HTMLProps<HTMLDivElement>
}) => {
  const theme = useTheme()

  return (
    <div
      style={{
        paddingRight: 8,
        maxHeight: 380,
        overflow: "auto",
        scrollbarColor: `${theme.palette.background.area} ${theme.palette.background.behind}`,
        scrollbarWidth: "thin",
        ...props.style,
      }}
    >
      <h3
        style={{
          position: "sticky",
          top: 0,
          marginTop: 0,
          marginBottom: 8,
          fontWeight: "normal",
          backgroundColor: theme.palette.background.behind,
        }}
      >
        {props.name}
      </h3>
      <div
        {...props.innerProps}
        style={{
          display: "grid",
          gridGap: 8,
          ...props.innerProps?.style,
        }}
      >
        {props.children}
      </div>
    </div>
  )
}

export const UndoHistory = () => {
  const theme = useTheme()
  const boardStore = boardContext.useStore()
  const undoHistory = boardContext.useStateSlice(
    board => [...board.undoHistory].reverse(),
    { isEqual: (_a, _b, a, b) => a.undoHistory === b.undoHistory },
  )

  return (
    <SidebarGroup
      name={
        <div style={{ height: "1em" }}>
          {undoHistory.length > 0 ? <span>History</span> : null}
        </div>
      }
    >
      {undoHistory.map(event => (
        <div
          key={event.id}
          style={{
            display: "grid",
            gridTemplateColumns: "1fr auto",
            gridTemplateRows: "auto auto",
            backgroundColor: theme.palette.background.behind,
          }}
        >
          <span>
            {event.description} issue #{event.issue.id}
          </span>
          <span
            style={{
              overflow: "hidden",
              fontSize: "0.9em",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              opacity: 0.9,
            }}
          >
            {event.issue.subject}
          </span>
          <Button
            style={{
              gridColumn: 2,
              gridRow: "1 / span 2",
            }}
            onClick={() => boardStore.undo(event)}
          >
            Undo
          </Button>
        </div>
      ))}
    </SidebarGroup>
  )
}
