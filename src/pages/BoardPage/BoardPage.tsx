import { ApiError } from "@app/api/api"
import { IssueAction, boardContext } from "@app/stores/BoardStore"
import { projectContext } from "@app/stores/ProjectStore"
import { redmineContext } from "@app/stores/RedmineStore"
import { Select } from "@app/styles"
import type { Status, Version } from "@app/types"
import { DragDropContext, DropResult } from "@pajn/react-beautiful-dnd"
import * as React from "react"
import { DndProvider } from "react-dnd"
import TouchBackend from "react-dnd-touch-backend"
import { Route, Routes, useNavigate, useParams } from "react-router-dom"
import { BoardView } from "./BoardView"
import { Dropzone } from "./Dropzone"
import { SidebarGroup, UndoHistory } from "./SidebarItems"
import { DraggableAvatar, UserDragLayer } from "./userDnd"

const LazyIssuePage = React.lazy(() =>
  import(/* webpackChunkName: "IssuePage" */ "../IssuePage/IssuePage").then(
    module => ({
      default: module.IssuePage,
    }),
  ),
)

export const BoardPage = () => {
  const redmineStore = redmineContext.useStore()
  const boardStore = projectContext.useStateSlice(project => project.boardStore)

  const onDragEnd = React.useCallback(
    (result: DropResult) => {
      if (!boardStore) return

      const { draggableId, source, destination } = result

      if (!draggableId.startsWith("issue-")) {
        return
      }
      const issueId = +draggableId.slice("issue-".length)

      // Dropped outside the list
      if (!destination) {
        return
      }

      const [sourceParent, sourceColumn] = source.droppableId.split("-")

      const redmineData = redmineStore.getState().data!

      const [
        destinationParent,
        destinationColumn,
      ] = destination.droppableId.split("-")

      const modifications: Array<IssueAction> = []

      if (sourceParent !== destinationParent) {
        modifications.push({
          type: "setParentIssue",
          parent: +destinationParent,
          index: destination.index,
        })
      }

      if (sourceColumn !== destinationColumn) {
        modifications.push({
          type: "setStatus",
          status: redmineData.statuses.find(s => s.name === destinationColumn)!,
          index: destination.index,
        })
      }

      if (
        sourceParent === destinationParent &&
        sourceColumn === destinationColumn &&
        source.index !== destination.index
      ) {
        modifications.push({
          type: "reorder",
          fromIndex: source.index,
          toIndex: destination.index,
        })
      }

      if (modifications.length > 0) {
        boardStore
          .updateIssue({ issueId, modifications })
          .catch(async error => {
            console.error("Error updating issue after DnD", error)
            if (error instanceof ApiError && error.response.status === 422) {
              const response = await error.response.json()
              alert(
                "Error updating issue: " + (response?.errors ?? []).join(", "),
              )
            } else {
              alert("Error updating issue")
            }
          })
      }
    },
    [boardStore, redmineStore],
  )

  const navigate = useNavigate()
  const projects = redmineContext.useStateSlice(
    api =>
      api.data?.projects.sort((a, b) => a.name.localeCompare(b.name)) ?? [],
    { isEqual: (_a, _b, a, b) => a?.data?.projects === b?.data?.projects },
  )
  const versionName = decodeURIComponent(useParams().versionName ?? "")
  const projectName = projectContext.useStateSlice(project => project.name)
  const destructiveStatuses = projectContext.useStateSlice(
    project => project.destructiveStatuses,
  )
  const versions = projectContext.useStateSlice(
    project => project.versions.sort((a, b) => a.name.localeCompare(b.name)),
    { isEqual: (_a, _b, a, b) => a.versions === b.versions },
  )
  const members = projectContext.useStateSlice(
    project =>
      Array.from(project.members.values()).sort((a, b) =>
        a.name.localeCompare(b.name),
      ),
    { isEqual: (_a, _b, a, b) => a.members === b.members },
  )

  if (versions.length > 0 && !versionName) {
    setTimeout(() => {
      const openVersions = versions.filter(v => v.isOpen)
      const latestVersion =
        openVersions.length > 0
          ? openVersions[openVersions.length - 1]
          : versions[versions.length - 1]
      navigate(encodeURIComponent(latestVersion.name), {
        replace: true,
      })
    })
  }

  const onStatusDrop = (status: Pick<Status, "id" | "name">) => (
    issueId: number,
  ) => {
    if (!boardStore) return
    boardStore.updateIssue({
      issueId,
      modifications: [
        {
          type: "setStatus",
          status,
          index: 0,
        },
      ],
    })
  }

  const onVersionDrop = (version: Version | undefined) => (issueId: number) => {
    if (!boardStore) return
    boardStore.updateIssue({
      issueId,
      modifications: [
        {
          type: "setVersion",
          version,
        },
      ],
    })
  }

  return (
    <DndProvider backend={TouchBackend} options={{ enableMouseEvents: true }}>
      <DragDropContext onDragEnd={onDragEnd}>
        <div
          style={{
            display: "grid",
            gridTemplateRows: "80px 1fr",
            gridTemplateColumns: "1fr 250px",
            gridGap: 8,
            padding: 16,
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <h1 style={{ flex: 1, margin: 32 }}>{projectName}</h1>
            <Select
              aria-label="Version"
              value={versionName}
              onChange={e => {
                const version = versions.find(v => v.name === e.target.value)
                if (version) {
                  navigate(`../${encodeURIComponent(version.name)}`, {
                    replace: true,
                  })
                }
              }}
            >
              {!versionName && <option disabled value=""></option>}
              {versions.map(version => (
                <option key={version.id} value={version.name}>
                  {version.name}
                </option>
              ))}
            </Select>
          </div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Select
              style={{ maxWidth: "100%" }}
              aria-label="Project"
              value={projectName}
              onChange={e => {
                const project = projects.find(v => v.name === e.target.value)
                if (project) {
                  navigate(`/project/${encodeURIComponent(project.slug)}`, {
                    replace: true,
                  })
                }
              }}
            >
              {!projectName && <option disabled value=""></option>}
              {projects.map(project => (
                <option key={project.id} value={project.name}>
                  {project.name}
                </option>
              ))}
            </Select>
          </div>
          {boardStore ? (
            <boardContext.Provider store={boardStore}>
              <BoardView />
              <React.Suspense fallback={null}>
                <Routes>
                  <Route path=":issueId" element={<LazyIssuePage />} />
                  <Route path="" element={<LazyIssuePage />} />
                </Routes>
              </React.Suspense>
            </boardContext.Provider>
          ) : (
            <div />
          )}
          <div
            onMouseEnter={() => {
              ;(window as any).rbdScrollBlocked = true
            }}
            onMouseLeave={() => {
              ;(window as any).rbdScrollBlocked = false
            }}
          >
            <div
              style={{
                position: "sticky",
                top: 8,
                display: "grid",
                alignContent: "start",
                gridGap: 24,
                paddingLeft: 8,
              }}
            >
              <SidebarGroup
                name="Members"
                style={{
                  overflow: "visible",
                  maxHeight: "initial",
                }}
                innerProps={{
                  style: {
                    display: "flex",
                    flexWrap: "wrap",
                    justifyContent: "center",
                  },
                }}
              >
                {members.map(user => (
                  <DraggableAvatar key={user.id} user={user} size={40} />
                ))}
              </SidebarGroup>
              <SidebarGroup name="Status">
                {destructiveStatuses.map(status => (
                  <Dropzone key={status.name} onDrop={onStatusDrop(status)}>
                    {status.name}
                  </Dropzone>
                ))}
              </SidebarGroup>
              <SidebarGroup name="Version">
                <Dropzone onDrop={onVersionDrop(undefined)}>none</Dropzone>
                {versions
                  .filter(
                    version => version.isOpen && version.name !== versionName,
                  )
                  .map(version => (
                    <Dropzone
                      key={version.name}
                      onDrop={onVersionDrop(version)}
                    >
                      {version.name}
                    </Dropzone>
                  ))}
              </SidebarGroup>
              {boardStore && (
                <boardContext.Provider store={boardStore}>
                  <UndoHistory />
                </boardContext.Provider>
              )}
              <div style={{ height: 16 }} />
            </div>
          </div>
        </div>
        <UserDragLayer />
      </DragDropContext>
    </DndProvider>
  )
}
