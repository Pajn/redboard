import TextareaAutosize from "@pajn/react-autosize-textarea"
import * as React from "react"
import { Link } from "react-router-dom"
import styled from "styled-components"

const SmartLink = (
  props: React.AnchorHTMLAttributes<HTMLAnchorElement> & { to?: string },
  // eslint-disable-next-line jsx-a11y/anchor-has-content
) => (props.href ? <a {...props} /> : <Link {...props} />)

export type Palette = typeof darkPalette
export type Theme = {
  palette: Palette
  board: {
    background: string
    card: string
    headerDivider: string
    swimlaneDivider: string
  }
  input: {
    normal: string
    focus: string
  }
  button: {
    normal: string
    focus: string
    active: string
  }
  iconButton: {
    icon: string
    normal: string
    focus: string
    active: string
  }
}

declare module "styled-components" {
  export interface DefaultTheme extends Theme {}
}

export const darkPalette = {
  background: {
    behind: "#282c34",
    area: "hsl(243, 10%, 25%)",
    above: "hsl(243, 10%, 35%)",
    focus: "hsl(243, 10%, 45%)",
    active: "hsl(243, 10%, 50%)",
  },
  text: {
    main: "white",
    secondary: "#eee",
    error: "hsl(365, 100%, 65%)",
  },
  divider: {
    strong: "rgba(255, 255, 255, 0.3)",
    weak: "rgba(255, 255, 255, 0.1)",
  },
}
export const lightPalette: Palette = {
  background: {
    behind: "#282c34", // todo
    area: "white",
    above: "hsl(243, 10%, 35%)", // todo
    focus: "hsl(243, 10%, 45%)", // todo
    active: "hsl(243, 10%, 50%)", // todo
  },
  text: {
    main: "rgba(0, 0, 0, 0.87)",
    secondary: "rgba(0, 0, 0, 0.6)",
    error: "hsl(365, 100%, 65%)",
  },
  divider: {
    strong: "rgba(0, 0, 0, 0.3)",
    weak: "rgba(0, 0, 0, 0.1)",
  },
}

type DeepPartial<T> = T extends object
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : T
export const createTheme = (
  palette: Palette,
  _overrides?: DeepPartial<Theme>,
): Theme => ({
  palette,
  board: {
    background: palette.background.area,
    card: palette.background.above,
    headerDivider: palette.divider.strong,
    swimlaneDivider: palette.divider.weak,
  },
  input: {
    normal: palette.background.above,
    focus: palette.background.focus,
  },
  button: {
    normal: palette.background.above,
    focus: palette.background.focus,
    active: palette.background.active,
  },
  iconButton: {
    icon: palette.text.secondary,
    normal: "transparent",
    focus: palette.background.focus,
    active: palette.background.active,
  },
})

export const darkTheme = createTheme(darkPalette)
export const light = createTheme(lightPalette, {
  iconButton: {
    focus: "radial-gradient(rgba(0, 0, 0, 0.1), transparent)",
    active:
      "radial-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.15), transparent)",
  },
})

export const centerContent = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}

export const fill = {
  display: "absolue",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
}

export const arrowUp = (size: number, color: string) => ({
  width: 0,
  height: 0,
  borderLeft: `${size}px solid transparent`,
  borderRight: `${size}px solid transparent`,
  borderBottom: `${size}px solid ${color}`,
})

export const cssJoin = (
  ...transitions: Array<string | null | undefined | false>
) => transitions.filter(t => t && t !== "none").join(", ")

export const Button = styled.button(({ theme }) => ({
  display: "block",
  padding: "4px 8px",
  textDecoration: "none",
  border: "none",
  borderRadius: 5,
  color: "inherit",
  background: theme.button.normal,
  outline: "none",
  cursor: "pointer",

  transition: "opacity 200ms, background 100ms",

  "&:hover, &.focus-visible": {
    background: theme.button.focus,
  },
  "&:active": {
    background: theme.button.active,
  },
  "&:disabled": {
    background: theme.button.normal,
    cursor: "default",
    opacity: 0.5,
  },
}))

export const LinkButton = Button.withComponent(SmartLink)

export const IconButton = styled.button.attrs({ type: "button" })(
  ({ theme }) => ({
    ...centerContent,
    margin: 6,
    padding: 0,
    width: 36,
    height: 36,
    fontSize: 22,
    border: "none",
    borderRadius: "50%",
    color: theme.iconButton.icon,
    background: theme.iconButton.normal,
    outline: "none",
    cursor: "pointer",

    transition: "opacity 200ms, background 100ms",

    "&:hover, &.focus-visible": {
      background: theme.iconButton.focus,
    },
    "&:active": {
      background: theme.iconButton.active,
    },
    "&:disabled": {
      background: theme.button.normal,
      opacity: 0.5,
      cursor: "default",
    },
  }),
)

export const IconLinkButton = IconButton.withComponent(SmartLink)

export const Label = styled.label(({ theme }) => ({
  fontSize: 16,

  "span:first-of-type": {
    display: "inline-block",
    marginBottom: 2,
  },

  "span:nth-of-type(2)": {
    display: "inline-block",
    marginTop: 4,
    marginLeft: 4,
    fontSize: "0.9em",
    color: theme.palette.text.error,
  },

  "input, select, textarea": {
    width: "100%",
  },
}))

export const Input = styled.input(({ theme }) => ({
  boxSizing: "border-box",
  padding: "4px 8px",
  fontSize: 16,
  border: "none",
  borderRadius: 5,
  color: "inherit",
  background: theme.input.normal,
  outline: "none",

  transition: "opacity 200ms, background 100ms",

  "&:hover, &.focus-visible": {
    background: theme.input.focus,
  },
  "&:disabled": {
    background: theme.button.normal,
    opacity: 0.5,
  },
}))

export const Select = styled.select(({ theme }) => ({
  MozAppearance: "none",
  appearance: "none",
  padding: "4px 8px",
  textAlign: "center",
  border: "none",
  borderRadius: 5,
  color: "inherit",
  background: theme.input.normal,
  outline: "none",
  cursor: "pointer",

  transition: "opacity 200ms, background 100ms",

  "&:hover, &.focus-visible": {
    background: theme.input.focus,
  },
  "&:-moz-focusring": {
    color: "transparent",
    textShadow: "0 0 0 white",
  },
  "&:disabled": {
    background: theme.button.normal,
    opacity: 0.5,
    cursor: "default",
  },
}))

export const Textarea = styled(TextareaAutosize).attrs({ async: true })(
  ({ theme }) => ({
    boxSizing: "border-box",
    padding: "4px 8px",
    fontSize: 16,
    border: "none",
    borderRadius: 5,
    color: "inherit",
    background: theme.input.normal,
    outline: "none",
    resize: "none",

    transition: "opacity 200ms, background 100ms",

    "&:hover, &.focus-visible": {
      background: theme.input.focus,
    },
    "&:disabled": {
      background: theme.button.normal,
      opacity: 0.5,
    },
  }),
)
