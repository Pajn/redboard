import * as immutable from "immutable"
import { asArray, filter, map, sort } from "iterates/sync"
import { pipeValue } from "iterates/utils"
import type {
  Board,
  ChangedPropertyKey,
  Clearable,
  FullIssue,
  HistoryEvent,
  Issue,
  NewIssue,
  Priority,
  Project,
  Status,
  Tracker,
  User,
  Version,
} from "../types"

class Cache<K, T extends {}> {
  private store = new Map<K, T>()

  constructor(private getId: (item: T) => K) {}

  static default<T extends { id: number | string }>(): Cache<T["id"], T> {
    return new Cache(item => item.id)
  }

  put = (item: T): T => {
    this.store.set(this.getId(item), item)
    return item
  }

  get = (id: K): T | undefined => {
    return this.store.get(id)
  }
}

export type ApiClient = ReturnType<typeof createApiClient>

export type ApiSettings = {
  baseUrl?: string
  apiKey?: string
  customMemberFields: Array<string>
  swimlaneTrackers: Array<string>
  hiddenStatuses: Array<string>
}

export class ApiError extends Error {
  constructor(
    readonly method: string,
    readonly path: string,
    readonly response: Response,
  ) {
    super(`${response.status} - ${response.statusText}`)
  }
}

export function createApiClient(getSettings: () => ApiSettings) {
  type CallOptions = {
    method?: RequestInit["method"]
    credentials?: { username: string; password: string }
    query?: Record<string, string | number>
    body?: object
    ignoreResponse?: boolean
  }
  const call = async (path: string, options: CallOptions = {}) => {
    const { baseUrl, apiKey } = getSettings()
    if (!baseUrl) throw Error("baseUrl is not set")
    const url = new URL(path, baseUrl)
    if (options.query) {
      for (const [key, value] of Object.entries(options.query)) {
        url.searchParams.set(key, value.toString())
      }
    }
    const response = await fetch(url.href, {
      method: options.method,
      headers: Object.assign(
        {
          Accept: "application/json",
        },
        options.credentials || apiKey
          ? {
              Authorization: `Basic ${btoa(
                options.credentials
                  ? `${options.credentials.username}:${options.credentials.password}`
                  : `${apiKey}:`,
              )}`,
            }
          : undefined,
        options.body ? { "Content-Type": "application/json" } : undefined,
      ),
      body: options.body ? JSON.stringify(options.body) : undefined,
    })

    if (!response.ok) {
      throw new ApiError(options.method ?? "GET", path, response)
    }

    return response.status === 204 || options.ignoreResponse
      ? null
      : response.json()
  }
  async function paginate<
    K extends string,
    T extends { [P in K]: Array<unknown> }
  >(key: K, path: string, options: CallOptions & { pageSize?: number } = {}) {
    type PaginationResponse = T & {
      total_count: number
      offset: number
      limit: number
    }
    let offset = 0
    const pageSize = options.pageSize ?? 100
    let values: Array<unknown> = []

    while (true) {
      const response: PaginationResponse = await call(path, {
        ...options,
        query: {
          ...options.query,
          offset,
          limit: pageSize,
        },
      })

      offset += pageSize
      values = values.concat(response[key])

      if (offset >= response.total_count) break
    }

    return values as T[K]
  }

  // Authentication

  async function login(
    username: string,
    password: string,
  ): Promise<User & { apiKey: string }> {
    type Response = {
      user: {
        id: number
        login: string
        firstname: string
        lastname: string
        mail: string
        created_on: string
        last_login_on: string
        api_key: string
      }
    }
    const response: Response = await call(`/users/current.json`, {
      credentials: { username, password },
    })

    return {
      id: response.user.id,
      username: response.user.login,
      name: `${response.user.firstname} ${response.user.lastname}`,
      apiKey: response.user.api_key,
    }
  }

  // Redmine data
  const priorities = Cache.default<Priority>()
  const statuses = Cache.default<Status>()
  const trackers = Cache.default<Tracker>()
  const users = Cache.default<User>()
  const versions = Cache.default<Version>()

  async function fetchPriorities(): Promise<Array<Priority>> {
    type Response = {
      issue_priorities: Array<{
        id: number
        name: string
        is_default: boolean
        active: boolean
      }>
    }
    const response: Response = await call(`/enumerations/issue_priorities.json`)

    return response.issue_priorities.map(priority =>
      priorities.put({
        id: priority.id,
        name: priority.name,
        isDefault: priority.is_default,
        active: priority.active,
      }),
    )
  }

  async function fetchProjects(): Promise<
    Array<{
      id: number
      name: string
      slug: string
    }>
  > {
    type Response = {
      projects: Array<{
        id: number
        name: string
        identifier: string
        description: string
        homepage: string
        parent: {
          id: number
          name: string
        }
        status: number
        trackers: Array<{
          id: number
          name: string
        }>
        created_on: string
        updated_on: string
      }>
    }

    const projects = await paginate<"projects", Response>(
      "projects",
      `/projects.json`,
    )

    return projects.map(project => ({
      id: project.id,
      name: project.name,
      slug: project.identifier,
    }))
  }

  async function fetchStatuses(): Promise<Array<Status>> {
    const { hiddenStatuses } = getSettings()
    type Response = {
      issue_statuses: Array<
        | {
            id: number
            name: string
            is_closed?: undefined
          }
        | {
            id: number
            name: string
            is_closed: boolean
          }
      >
    }
    const response: Response = await call(`/issue_statuses.json`)

    return response.issue_statuses
      .map(s =>
        statuses.put({
          id: s.id,
          name: s.name,
          isClosed: s.is_closed ?? false,
        }),
      )
      .filter(s => !hiddenStatuses.includes(s.name))
  }

  async function fetchTrackers(): Promise<Array<Tracker>> {
    type Response = {
      trackers: Array<{
        id: number
        name: string
        default_status: {
          id: number
          name: string
        }
      }>
    }
    const response: Response = await call(`/trackers.json`)

    return response.trackers.map(trackers.put)
  }

  // Project data

  async function fetchMembers(
    projectSlug: string,
  ): Promise<immutable.Map<number, User>> {
    type Response = {
      memberships: Array<{
        id: number
        project: {
          id: number
          name: string
        }
        user?: {
          id: number
          name: string
        }
        roles: Array<{
          id: number
          name: string
        }>
      }>
    }
    const memberships = await paginate<"memberships", Response>(
      "memberships",
      `/projects/${projectSlug}/memberships.json`,
    )

    return immutable.Map(
      memberships
        .filter(m => m.user)
        .map(m => [
          m.user!.id,
          users.put({
            id: m.user!.id,
            name: m.user!.name,
          }),
        ]),
    )
  }

  async function fetchVersions(projectSlug: string): Promise<Array<Version>> {
    type Response = {
      versions: Array<{
        id: number
        project: {
          id: number
          name: string
        }
        name: string
        description: string
        status: string
        sharing: string
        created_on: string
        updated_on: string
      }>
    }
    const response: Response = await call(
      `/projects/${projectSlug}/versions.json`,
    )

    return response.versions.map(v =>
      versions.put({ id: v.id, name: v.name, isOpen: v.status === "open" }),
    )
  }

  async function fetchSimpleProject(projectId: number) {
    type Response = {
      project: {
        id: number
        name: string
        identifier: string
        description: string
        homepage: string
        parent: {
          id: number
          name: string
        }
        status: number
        created_on: string
        updated_on: string
      }
    }
    const response = (await call(`/projects/${projectId}.json`)) as Response

    return response.project
  }

  async function fetchProject(
    redmineData: { statuses: Array<Status>; trackers: Array<Tracker> },
    slug: string,
  ): Promise<Project> {
    type Response = {
      project: {
        id: number
        name: string
        identifier: string
        description: string
        homepage: string
        parent: {
          id: number
          name: string
        }
        status: number
        trackers: Array<{
          id: number
          name: string
        }>
        created_on: string
        updated_on: string
      }
    }

    const [members, { project }, versions] = await Promise.all([
      fetchMembers(slug),
      call(`/projects/${slug}.json`, {
        query: { include: "trackers" },
      }) as Promise<Response>,
      fetchVersions(slug),
    ])

    return {
      id: project.id,
      name: project.name,
      slug: project.identifier,
      columns: redmineData.statuses.filter(s => !s.isClosed),
      destructiveStatuses: redmineData.statuses.filter(s => s.isClosed),
      members,
      trackers: redmineData.trackers.filter(t =>
        project.trackers.some(pt => pt.id === t.id),
      ),
      versions,
    }
  }

  const mapIssue = (issue: ApiIssue): Issue => ({
    id: issue.id,
    projectId: issue.project.id,
    parent: issue.parent?.id,
    tracker: issue.tracker,
    subject: issue.subject,
    description: issue.description ?? "",
    priority: issue.priority,
    status: issue.status,
    version: issue.fixed_version,
    assignee: issue.assigned_to?.id,
    attachments:
      issue.attachments?.map(attachment => ({
        id: attachment.id,
        filename: attachment.filename,
        filesize: attachment.filesize,
        contentType: attachment.content_type,
        description: attachment.description,
        contentUrl: attachment.content_url,
        thumbnailUrl: attachment.thumbnail_url,
        author: attachment.author,
        createdOn: new Date(attachment.created_on),
      })) ?? [],
    customFields: issue.custom_fields ?? [],
    createdOn: new Date(issue.created_on),
    createdBy: issue.author,
    updatedOn: new Date(issue.updated_on),
  })

  async function fetchBoard(
    project: Project,
    version: Version,
  ): Promise<Board> {
    const { swimlaneTrackers } = getSettings()
    type Response = {
      issues: Array<ApiIssue>
    }
    const response = await paginate<"issues", Response>(
      "issues",
      `/issues.json`,
      {
        query: {
          include: "attachments",
          project_id: project.id,
          fixed_version_id: version.id,
        },
      },
    )
    let issues = immutable.Map(
      response.map((issue): [number, Issue] => [issue.id, mapIssue(issue)]),
    )

    const parentIssues = new Set()
    const missingParents = new Set()
    for (const issue of issues.values()) {
      if (issue.parent !== undefined) {
        parentIssues.add(issue.parent)
        if (!issues.has(issue.parent)) {
          missingParents.add(issue.parent)
        }
      }
    }

    if (missingParents.size > 0) {
      const response = await paginate<"issues", Response>(
        "issues",
        `/issues.json`,
        {
          query: {
            include: "attachments",
            issue_id: [...missingParents].join(","),
          },
        },
      )
      issues = issues.withMutations(issues => {
        response.forEach(issue => issues.set(issue.id, mapIssue(issue)))
      })
    }

    return {
      issues,
      structure: pipeValue(
        issues.values(),
        filter(
          i =>
            parentIssues.has(i.id) || swimlaneTrackers.includes(i.tracker.name),
        ),
        map(
          swimlane =>
            [
              swimlane,
              immutable.Map(
                project.columns.map(c => [
                  c.name,
                  pipeValue(
                    issues.values(),
                    filter(
                      i => i.parent === swimlane.id && i.status.name === c.name,
                    ),
                    map(i => i.id),
                    sort((a, b) => a - b),
                    asArray,
                  ),
                ]),
              ),
            ] as const,
        ),
        sort(([a], [b]) => a.id - b.id),
        asArray,
      ),
    }
  }

  async function fetchIssue(issueId: number): Promise<Issue | null> {
    type Response = {
      issue: ApiIssue
    }
    try {
      const response: Response = await call(`/issues/${issueId}.json`)

      return response.issue ? mapIssue(response.issue) : null
    } catch (err) {
      if (err instanceof ApiError && err.response.status === 404) {
        return null
      }
      throw err
    }
  }

  async function fetchFullIssue(issueId: number): Promise<FullIssue | null> {
    type Response = {
      issue: ApiIssue
    }
    type ChildrenResponse = {
      issues: Array<ApiIssue>
    }
    try {
      const [response, children] = await Promise.all([
        call(`/issues/${issueId}.json`, {
          query: {
            include: "attachments,journals",
          },
        }) as Promise<Response>,
        paginate<"issues", ChildrenResponse>("issues", `/issues.json`, {
          query: {
            parent_id: issueId,
            status_id: "*",
          },
        }),
      ])

      const mapChangedProperty = (
        details: JournalDetails,
      ): [HistoryEvent["changes"][0]] | [] => {
        if (
          (details.new_value == null || details.new_value === "") &&
          (details.old_value == null || details.old_value === "")
        )
          return []

        if (details.property === "attachment") {
          return [
            {
              type: "attachment",
              action: details.new_value ? "added" : "removed",
              id: +details.name,
              filename: details.new_value ?? details.old_value!,
            },
          ]
        }

        if (details.property !== "attr") return []

        function changedProperty<P extends ChangedPropertyKey>(
          property: P,
          mapValue: (value: string) => Required<Issue>[P],
        ): [HistoryEvent["changes"][0]] {
          return [
            {
              type: "property",
              action: (details.new_value == null || details.new_value === ""
                ? "removed"
                : details.old_value == null || details.old_value === ""
                ? "added"
                : "updated") as "updated", // Need to cast because Typescript handels unions badly
              property,
              newValue: mapValue(details.new_value!),
              oldValue: mapValue(details.old_value!),
            },
          ]
        }

        switch (details.name) {
          case "subject":
            return changedProperty("subject", subject => subject)
          case "description":
            return changedProperty("description", description => description)
          case "priority_id":
            return changedProperty(
              "priority",
              id => priorities.get(+id) ?? { id: +id, name: id },
            )
          case "status_id":
            return changedProperty(
              "status",
              id => statuses.get(+id) ?? { id: +id, name: id },
            )
          case "tracker_id":
            return changedProperty(
              "tracker",
              id => trackers.get(+id) ?? { id: +id, name: id },
            )
          case "project":
            return changedProperty("projectId", id => +id)
          case "fixed_version_id":
            return changedProperty(
              "version",
              id => versions.get(+id) ?? { id: +id, name: id },
            )
          case "parent_id":
            return changedProperty("parent", id => +id)
          case "assigned_to_id":
            return changedProperty("assignee", id => +id)
          default:
            console.warn("Ignoring changed property", details.name, details)
            return []
        }
      }

      const history: FullIssue["history"] =
        response.issue.journals
          ?.map(event => ({
            id: event.id,
            notes: event.notes ?? "",
            user: event.user,
            createdOn: new Date(event.created_on),
            changes: event.details.flatMap(mapChangedProperty),
          }))
          .filter(
            event => event.notes.length > 0 || event.changes.length > 0,
          ) ?? []

      return response.issue
        ? {
            ...mapIssue(response.issue),
            comments: history.filter(h => h.notes.length > 0),
            history,
            children: children.map(mapIssue) ?? [],
          }
        : null
    } catch (err) {
      if (err instanceof ApiError && err.response.status === 404) {
        return null
      }
      throw err
    }
  }

  async function searchIssues(
    projectId: number,
    query: string,
  ): Promise<Array<Issue>> {
    type Response = {
      issues: Array<ApiIssue>
    }
    const [
      { issues: idQuery } = { issues: [] },
      { issues: subjectQuery },
    ] = await Promise.all([
      !isNaN(+query)
        ? (call(`/issues.json`, {
            query: {
              issue_id: query,
            },
          }) as Promise<Response>)
        : undefined,
      call(`/issues.json`, {
        query: {
          project_id: projectId,
          subject: `~${query.replace(/[^\w ]/g, "")}`,
          limit: 50,
        },
      }) as Promise<Response>,
    ])

    return idQuery.concat(subjectQuery).map(mapIssue)
  }

  async function createIssue(issue: NewIssue) {
    type Responese = { issue: ApiIssue }
    const response: Responese = await call(`/issues.json`, {
      method: "POST",
      body: {
        issue: {
          project_id: issue.projectId,
          tracker_id: issue.tracker.id,
          subject: issue.subject,
          description: issue.description,
          priority_id: issue.priority.id,
          status_id: issue.status.id,
          parent_issue_id: issue.parent,
          fixed_version_id: issue.version?.id,
          assigned_to_id: issue.assignee,
        },
      },
    })

    return mapIssue(response.issue)
  }

  async function updateIssue(
    id: number,
    comment: string | undefined,
    changes: Partial<Clearable<Issue>>,
  ) {
    await call(`/issues/${id}.json`, {
      method: "PUT",
      body: {
        issue: Object.assign(
          {},
          changes.projectId === undefined
            ? undefined
            : { project_id: changes.projectId },
          changes.tracker === undefined
            ? undefined
            : { tracker_id: changes.tracker.id },
          changes.subject === undefined
            ? undefined
            : { subject: changes.subject },
          changes.description === undefined
            ? undefined
            : { description: changes.description },
          changes.priority === undefined
            ? undefined
            : { priority_id: changes.priority.id },
          changes.status === undefined
            ? undefined
            : { status_id: changes.status.id },
          changes.parent === undefined
            ? undefined
            : { parent_issue_id: changes.parent },
          changes.version === undefined
            ? undefined
            : { fixed_version_id: changes.version?.id ?? null },
          changes.assignee === undefined
            ? undefined
            : { assigned_to_id: changes.assignee ?? "" },
          changes.customFields && { custom_fields: changes.customFields },
          comment === undefined ? undefined : { notes: comment },
        ),
      },
      ignoreResponse: true,
    })
  }

  function getUser(userId: number): User | undefined {
    return users.get(userId)
  }

  return {
    login,
    fetchPriorities,
    fetchProjects,
    fetchStatuses,
    fetchTrackers,
    fetchSimpleProject,
    fetchProject,
    fetchBoard,
    fetchIssue,
    fetchFullIssue,
    searchIssues,
    createIssue,
    updateIssue,
    getUser,
  }
}

type ApiIssue = {
  id: number
  project: {
    id: number
    name: string
  }
  tracker: {
    id: number
    name: string
  }
  status: {
    id: number
    name: string
  }
  priority: {
    id: number
    name: string
  }
  author: {
    id: number
    name: string
  }
  assigned_to?: {
    id: number
    name: string
  }
  fixed_version?: {
    id: number
    name: string
  }
  parent?: {
    id: number
  }
  subject: string
  description?: string
  start_date: string
  done_ratio: number
  custom_fields?: Array<{
    id: number
    name: string
    value: string
  }>
  created_on: string
  updated_on: string
  attachments?: Array<{
    id: number
    filename: string
    filesize: number
    content_type: string
    description: string
    content_url: string
    thumbnail_url: string
    author: {
      id: number
      name: string
    }
    created_on: string
  }>
  journals?: Array<{
    id: number
    user: {
      id: number
      name: string
    }
    notes?: string
    private_notes?: boolean
    created_on: string
    details: Array<JournalDetails>
  }>
  children?: Array<{
    id: number
    tracker: {
      id: number
      name: string
    }
    subject: string
  }>
}

type JournalDetails = {
  property: "attachment" | "attr"
  name: string
  old_value?: string | null
  new_value?: string | null
}
