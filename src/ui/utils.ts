import * as React from "react"

export type MutableRef<T> =
  | React.RefCallback<T>
  | React.MutableRefObject<T>
  | null
  | undefined

/**
 * passes {value} to {ref}
 *
 * WARNING: Be sure to only call this inside a callback that is passed as a ref.
 * Otherwise make sure to cleanup previous {ref} if it changes. See
 * https://github.com/mui-org/material-ui/issues/13539
 *
 * useful if you want to expose the ref of an inner component to the public api
 * while still using it inside the component
 *
 * @param ref a ref callback or ref object if anything falsy this is a no-op
 */
export function setRef<T>(ref: MutableRef<T>, value: T) {
  if (typeof ref === "function") {
    ref(value)
  } else if (ref) {
    ref.current = value
  }
}

export function useForkRef<T>(refA: MutableRef<T>, refB: MutableRef<T>) {
  /**
   * This will create a new function if the ref props change and are defined.
   * This means react will call the old forkRef with `null` and the new forkRef
   * with the ref. Cleanup naturally emerges from this behavior
   */
  return React.useMemo(() => {
    if (refA == null && refB == null) {
      return null
    }
    return (refValue: T) => {
      setRef(refA, refValue)
      setRef(refB, refValue)
    }
  }, [refA, refB])
}

function ownerDocument(node: any) {
  return (node && node.ownerDocument) || document
}

export function ownerWindow(node: any): Window {
  const doc = ownerDocument(node)
  return doc.defaultView || window
}

export const useEnhancedEffect =
  typeof window !== "undefined" ? React.useLayoutEffect : React.useEffect

export const elementHasFocus = (element: Element | null) =>
  element !== null &&
  element.parentElement?.querySelector(":focus-within") === element

export const useAutofocus = (deps: Array<unknown> = []) => {
  const ref = React.useRef<HTMLElement | null>(null)
  const callbackRef = React.useCallback((e: HTMLElement | null) => {
    ref.current = e
  }, [])

  React.useEffect(() => {
    const element = ref.current
    if (element) {
      const autofocusElement =
        (element.querySelector("[data-autofocus]") as HTMLElement) ?? element

      autofocusElement.focus()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, deps)

  return callbackRef
}
