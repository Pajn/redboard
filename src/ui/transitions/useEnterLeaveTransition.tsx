import { useCallback, useEffect, useRef, useState } from "react"
import { AnimatableElement, requestNextFrame, useCurrentValue } from "./utils"

export type EnterLeaveStates =
  | "will-enter"
  | "entering"
  | "entered"
  | "will-exit"
  | "exiting"
  | "exited"

export type EnterLeaveTransitionController<E = AnimatableElement> = {
  ref: (element: E | null) => void
  state: EnterLeaveStates
  in: boolean
  mount: boolean
  isSetup: boolean
  isAnimating: boolean
  isFinished: boolean
}

export function useEnterLeaveTransition<E extends Element = AnimatableElement>(
  options: {
    in: boolean
    animateInitial?: boolean
  },
  controller: (
    element: E,
    controller: Readonly<EnterLeaveTransitionController<E>>,
  ) => void,
) {
  const controllerRef = useCurrentValue(controller)
  const stateRef = useRef({
    element: null,
    mount: options.in,
    isSetup: false,
    isAnimating: false,
    isFinished: true,
    ref: e => {
      if (e) {
        if (stateRef.current.element !== e) {
          if (stateRef.current.element != null) {
            stateRef.current.element.removeEventListener(
              "transitionend",
              onTransitionEnd,
            )
          }
          stateRef.current.element = e
          e.addEventListener("transitionend", onTransitionEnd)
          setState(stateRef.current.state)
        }
      } else if (stateRef.current.element) {
        stateRef.current.element.removeEventListener(
          "transitionend",
          onTransitionEnd,
        )
        stateRef.current.element = null
        setState("exited")
      }
    },
  } as EnterLeaveTransitionController<E> & { element: E | null })
  const [, rerender] = useState<any>()

  const setState = useCallback(
    (state: EnterLeaveStates) => {
      stateRef.current.state = state
      stateRef.current.in =
        state === "entering" || state === "entered" || state === "will-exit"
      const oldMount = stateRef.current.mount
      stateRef.current.mount = state !== "exited"
      stateRef.current.isSetup = state === "will-enter" || state === "will-exit"
      stateRef.current.isAnimating = state === "entering" || state === "exiting"
      stateRef.current.isFinished = state === "entered" || state === "exited"

      if (stateRef.current.element) {
        controllerRef.current(stateRef.current.element, stateRef.current)
      }

      if (oldMount !== stateRef.current.mount) {
        rerender({})
      }
    },
    [controllerRef],
  )

  const onTransitionEnd = useCallback(
    (e: Event) => {
      if (e.target === e.currentTarget) {
        if (stateRef.current.state === "entering") {
          setState("entered")
        }
        if (stateRef.current.state === "exiting") {
          setState("exited")
        }
      }
    },
    [setState],
  )

  useEffect(() => {
    if (
      options.in &&
      (stateRef.current.state === "exiting" ||
        stateRef.current.state === "exited")
    ) {
      setState("will-enter")
      requestNextFrame(() => {
        setState("entering")
      })
    }
    if (
      !options.in &&
      (stateRef.current.state === "entering" ||
        stateRef.current.state === "entered")
    ) {
      setState("will-exit")
      requestNextFrame(() => {
        setState("exiting")
      })
    }
  }, [options.in, setState])

  useEffect(() => {
    return () => {
      if (stateRef.current.element) {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        stateRef.current.element.removeEventListener(
          "transitionend",
          onTransitionEnd,
        )
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (stateRef.current.state === undefined) {
    setState(options.in && !options.animateInitial ? "entered" : "exited")
  }

  if (options.in && !stateRef.current.mount) {
    stateRef.current.mount = true
  }

  return stateRef.current
}
