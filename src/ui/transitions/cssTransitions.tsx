import type { FlipEffect, TransitionPlayer } from "./useFlipTransition"
import { requestNextFrame } from "./utils"

type Primitive = string | number | boolean | undefined
type Value = Primitive | Array<Primitive> | Record<string, Primitive>
type ValueOrGetter<A, R extends Value> = R | ((arg: A) => R)

function get<A, R extends Value>(getter: ValueOrGetter<A, R>, arg: A): R {
  if (typeof getter === "function") {
    return getter(arg)
  } else {
    return getter
  }
}

// https://stackoverflow.com/a/49579497
type IfEquals<X, Y, A = X, B = never> = (<T>() => T extends X ? 1 : 2) extends <
  T
>() => T extends Y ? 1 : 2
  ? A
  : B

// https://stackoverflow.com/a/49579497
type WritableKeys<T> = {
  [P in keyof T]-?: IfEquals<
    { [Q in P]: T[P] },
    { -readonly [Q in P]: T[P] },
    P
  >
}[keyof T]

type StringFields<T> = {
  [P in keyof T]: T[P] extends string ? P : never
}[keyof T]

type CSSTransitionProperty = WritableKeys<
  Pick<CSSStyleDeclaration, StringFields<CSSStyleDeclaration>>
>

export function cssTransition<T, E extends SVGSVGElement | HTMLElement>(
  property: CSSTransitionProperty,
  format: (delta: T, previousValue: string) => string,
  options: ValueOrGetter<
    T,
    { duration: number; easing?: string; delay?: number }
  >,
): TransitionPlayer<T, unknown, E> {
  return (transition, delta: T) => {
    const element = transition.element
    const initialTransition = element.style.transition
    const initialPropertyValue = element.style[property]

    const propertyValue = format(delta, initialPropertyValue)
    element.style.transition = ""
    element.style[property] = propertyValue
    transition.onSetup?.()

    requestNextFrame(() => {
      const { duration, easing = "ease", delay = 0 } = get(options, delta)
      element.style.transition = `${property} ${duration}ms ${easing} ${delay}ms`
      element.style[property] = initialPropertyValue
      transition.onStart?.()

      const listener = (e: TransitionEvent) => {
        element.style.transition = initialTransition
        if (e.target === e.currentTarget && e.propertyName === property) {
          element.removeEventListener("transitionend", listener as any)
        }
        transition.onEnd?.()
      }
      element.addEventListener("transitionend", listener as any)
    })
  }
}

export const cssTranslate = (
  options: ValueOrGetter<
    { dx: number; dy: number },
    { duration: number; easing?: string; delay?: number }
  >,
) =>
  cssTransition(
    "transform",
    ({ dx, dy }, previousValue) =>
      `translate(${dx}px, ${dy}px) ${previousValue}`,
    options,
  )

export const cssScale = (
  options: ValueOrGetter<
    { dw: number; dh: number },
    { duration: number; easing?: string; delay?: number }
  >,
  transformOrigin?: string,
): TransitionPlayer<{ dw: number; dh: number }> => {
  const animate = cssTransition(
    "transform",
    ({ dw, dh }, previousValue) => `scale(${dw}, ${dh}) ${previousValue}`,
    options,
  )

  return (transition, delta) => {
    let originalOrigin = ""
    animate(
      transformOrigin
        ? {
            ...transition,
            onSetup() {
              originalOrigin = transition.element.style.transformOrigin
              transition.element.style.transformOrigin = transformOrigin
              transition.onSetup?.()
            },
            onEnd() {
              transition.element.style.transformOrigin = originalOrigin
              transition.onEnd?.()
            },
          }
        : transition,
      delta,
    )
  }
}

export function translateEffect<E extends SVGSVGElement | HTMLElement>(
  animate: TransitionPlayer<{ dx: number; dy: number }, DOMRect, E>,
): FlipEffect<DOMRect, E> {
  return {
    collect: e => e.getBoundingClientRect(),
    play: transition => {
      const dx = transition.firstValue.x - transition.lastValue.x
      const dy = transition.firstValue.y - transition.lastValue.y

      if (dx !== 0 || dy !== 0) {
        animate(transition, { dx, dy })
      }
    },
  }
}

export function scaleEffect<E extends SVGSVGElement | HTMLElement>(
  animate: TransitionPlayer<{ dw: number; dh: number }, DOMRect, E>,
): FlipEffect<DOMRect, E> {
  return {
    collect: e => e.getBoundingClientRect(),
    play: transition => {
      const dw = transition.firstValue.width / transition.lastValue.width
      const dh = transition.firstValue.height / transition.lastValue.height

      if (dw !== 1 || dh !== 1) {
        animate(transition, { dw, dh })
      }
    },
  }
}

export const translate = (
  transition: ValueOrGetter<
    { dx: number; dy: number },
    { duration: number; easing?: string; delay?: number }
  >,
) => translateEffect(cssTranslate(transition))
export const scale = (
  transition: ValueOrGetter<
    { dw: number; dh: number },
    { duration: number; easing?: string; delay?: number }
  >,
  transformOrigin?: string,
) => scaleEffect(cssScale(transition, transformOrigin))
export const noopFlip: FlipEffect<any, any> = {
  collect: () => undefined,
  play: () => {},
}
