import { ReactNode, useCallback } from "react"
import * as React from "react"
import {
  EnterLeaveTransitionController,
  useEnterLeaveTransition,
} from "./useEnterLeaveTransition"
import {
  FlipEffect,
  FlipTransitionController,
  useFlipTransition,
} from "./useFlipTransition"
import type { AnimatableElement } from "./utils"

export function EnterLeaveTransition(props: {
  in: boolean
  animateInitial?: boolean
  controller: (
    element: AnimatableElement,
    controller: Readonly<EnterLeaveTransitionController>,
  ) => void
  children:
    | React.ReactElement
    | ((
        transition: EnterLeaveTransitionController & {
          element: AnimatableElement | null
        },
      ) => ReactNode)
  keepMounted?: boolean
}) {
  const transition = useEnterLeaveTransition(
    { in: props.in, animateInitial: props.animateInitial },
    props.controller,
  )

  if (!transition.mount && !props.keepMounted) return null

  if (typeof props.children === "function") {
    return <>{props.children(transition)}</>
  } else {
    return React.cloneElement(props.children, {
      ref: transition.ref,
    })
  }
}
export const FlipTransition = function FlipTransition<T, E>(props: {
  innerRef?: (e: E | null) => void
  effect: FlipEffect<T, E>
  deps?: Array<unknown>
  children:
    | React.ReactElement
    | ((transition: FlipTransitionController<T, E>) => ReactNode)
}) {
  const transition = useFlipTransition<T, E>(props.effect, props.deps)

  const ref = useCallback(
    props.innerRef
      ? (e: E | null) => {
          transition.ref(e)
          props.innerRef?.(e)
        }
      : transition.ref,
    [props.innerRef, transition],
  )

  if (typeof props.children === "function") {
    return <>{props.children({ ...transition, ref })}</>
  } else {
    return React.cloneElement(props.children, { ref })
  }
}
