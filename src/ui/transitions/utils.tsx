import { useEffect, useRef } from "react"

export type AnimatableElement = SVGSVGElement | HTMLElement

export const requestNextFrame = (callback: () => void) => {
  requestAnimationFrame(() => {
    requestAnimationFrame(callback)
  })
}

export function useCurrentValue<T>(value: T) {
  const valueRef = useRef(value)
  valueRef.current = value
  return valueRef
}

export function useIsFirstFrame() {
  const ref = useRef(true)

  useEffect(() => {
    requestNextFrame(() => {
      ref.current = false
    })
  }, [])

  return ref.current
}

export function useFreezableValue<T>(
  freeze: boolean,
  value: T,
  initialValue = value,
): T {
  const ref = useRef(initialValue)

  if (!freeze) {
    ref.current = value
  }

  return ref.current
}
