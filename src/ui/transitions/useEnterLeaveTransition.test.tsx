import chai, { expect } from "@esm-bundle/chai"
import hooks from "@testing-library/react-hooks"
import sinon from "sinon"
import sinonChai from "sinon-chai"
import { useEnterLeaveTransition } from "./useEnterLeaveTransition"

chai.use(sinonChai)

describe("useEnterLeaveTransition", () => {
  let onTransitionEnd: undefined | ((e: TransitionEvent) => void)
  let element = {}
  const elementMock = () => (element as unknown) as HTMLElement
  let nextFrame = () => {}

  beforeEach(() => {
    let raf = () => {}
    window.requestAnimationFrame = cb => {
      const nextRaf = () => {
        cb(0)
        if (raf === nextRaf) {
          raf = () => {}
        }
      }
      raf = nextRaf
      return 0
    }
    nextFrame = () => {
      raf()
      raf()
    }
    element = {
      addEventListener: sinon.fake(
        (event: string, listener: typeof onTransitionEnd) => {
          if (event === "transitionend") {
            onTransitionEnd = listener
          }
        },
      ),
      removeEventListener: sinon.fake(
        (event: string, listener: typeof onTransitionEnd) => {
          if (event === "transitionend" && listener === onTransitionEnd) {
            onTransitionEnd = undefined
          }
        },
      ),
    }
  })

  it("should not transition in on initial mount", () => {
    const ctrl = sinon.fake()
    const hook = hooks.renderHook(() =>
      useEnterLeaveTransition({ in: true }, ctrl),
    )
    const transition = hook.result.current

    expect(transition.in).to.equal(true)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("entered")
    expect(ctrl).to.have.callCount(0)
  })

  it("should transition in on initial mount if animateInitial is set", () => {
    const ctrl = sinon.fake((element: HTMLElement, controller: any) => {
      expect(element).to.equal(elementMock())
      expect(controller).to.equal(transition)
    })
    const hook = hooks.renderHook(() =>
      useEnterLeaveTransition({ in: true, animateInitial: true }, ctrl),
    )
    const transition = hook.result.current

    expect(transition.in).to.equal(false)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("will-enter")
    expect(ctrl).to.have.callCount(0)

    transition.ref(elementMock())
    expect(ctrl).to.have.callCount(1)
    nextFrame()

    expect(transition.in).to.equal(true)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("entering")
    expect(ctrl).to.have.callCount(2)

    hooks.act(() => onTransitionEnd!({} as any))

    expect(transition.in).to.equal(true)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("entered")
    expect(ctrl).to.have.callCount(3)
  })

  it("should not transition out on initial mount", () => {
    const ctrl = sinon.fake()
    const hook = hooks.renderHook(() =>
      useEnterLeaveTransition({ in: false }, ctrl),
    )
    const transition = hook.result.current

    expect(transition.in).to.equal(false)
    expect(transition.mount).to.equal(false)
    expect(transition.state).to.equal("exited")
    expect(ctrl).to.have.callCount(0)
  })

  it("should not transition out on initial mount even if animateInitial is set", () => {
    const ctrl = sinon.fake()
    const hook = hooks.renderHook(() =>
      useEnterLeaveTransition({ in: false, animateInitial: true }, ctrl),
    )
    const transition = hook.result.current

    expect(transition.in).to.equal(false)
    expect(transition.mount).to.equal(false)
    expect(transition.state).to.equal("exited")
    expect(ctrl).to.have.callCount(0)
  })

  it("should transition in on rerenders", () => {
    const ctrl = sinon.fake((element: HTMLElement, controller: any) => {
      expect(element).to.equal(elementMock())
      expect(controller).to.equal(transition)
    })
    const hook = hooks.renderHook(
      props => useEnterLeaveTransition(props, ctrl),
      {
        initialProps: { in: false },
      },
    )
    const transition = hook.result.current

    hook.rerender({ in: true })

    expect(transition.in).to.equal(false)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("will-enter")
    expect(ctrl).to.have.callCount(0)

    transition.ref(elementMock())
    expect(ctrl).to.have.callCount(1)
    nextFrame()

    expect(transition.in).to.equal(true)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("entering")
    expect(ctrl).to.have.callCount(2)

    hooks.act(() => onTransitionEnd!({} as any))

    expect(transition.in).to.equal(true)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("entered")
    expect(ctrl).to.have.callCount(3)
  })

  it("should transition out on rerenders", () => {
    const ctrl = sinon.fake((element: HTMLElement, controller: any) => {
      expect(element).to.equal(elementMock())
      expect(controller).to.equal(transition)
    })
    const hook = hooks.renderHook(
      props => useEnterLeaveTransition(props, ctrl),
      {
        initialProps: { in: true },
      },
    )
    const transition = hook.result.current

    hook.rerender({ in: false })

    expect(transition.in).to.equal(true)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("will-exit")
    expect(ctrl).to.have.callCount(0)

    transition.ref(elementMock())
    expect(ctrl).to.have.callCount(1)
    nextFrame()

    expect(transition.in).to.equal(false)
    expect(transition.mount).to.equal(true)
    expect(transition.state).to.equal("exiting")
    expect(ctrl).to.have.callCount(2)

    hooks.act(() => onTransitionEnd!({} as any))

    expect(transition.in).to.equal(false)
    expect(transition.mount).to.equal(false)
    expect(transition.state).to.equal("exited")
    expect(ctrl).to.have.callCount(3)
  })

  it("should wait for a frame on will-enter", () => {
    const ctrl = sinon.fake((element: HTMLElement, controller: any) => {
      expect(element).to.equal(elementMock())
      expect(controller).to.equal(transition)
    })
    const hook = hooks.renderHook(
      props => useEnterLeaveTransition(props, ctrl),
      {
        initialProps: { in: false },
      },
    )
    const transition = hook.result.current

    hook.rerender({ in: true })

    expect(transition.state).to.equal("will-enter")
    expect(ctrl).to.have.callCount(0)

    transition.ref(elementMock())

    expect(transition.state).to.equal("will-enter")
    expect(ctrl).to.have.callCount(1)

    nextFrame()

    expect(transition.state).to.equal("entering")
    expect(ctrl).to.have.callCount(2)
  })

  it("should wait for a frame on will-exit", () => {
    const ctrl = sinon.fake((element: HTMLElement, controller: any) => {
      expect(element).to.equal(elementMock())
      expect(controller).to.equal(transition)
    })
    const hook = hooks.renderHook(
      props => useEnterLeaveTransition(props, ctrl),
      {
        initialProps: { in: true },
      },
    )
    const transition = hook.result.current

    hook.rerender({ in: false })

    expect(transition.state).to.equal("will-exit")
    expect(ctrl).to.have.callCount(0)

    transition.ref(elementMock())
    expect(ctrl).to.have.callCount(1)

    expect(transition.state).to.equal("will-exit")
    expect(ctrl).to.have.callCount(1)

    nextFrame()

    expect(transition.state).to.equal("exiting")
    expect(ctrl).to.have.callCount(2)
  })
})
