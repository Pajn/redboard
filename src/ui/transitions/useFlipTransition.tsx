import { useCallback, useLayoutEffect, useRef } from "react"
import type { AnimatableElement } from "./utils"

export type TransitionPlayer<T, U = unknown, E = AnimatableElement> = (
  transition: FlipTransition<U, E>,
  delta: T,
) => void

export type FlipEffect<T, E = AnimatableElement> = {
  collect: (element: E) => T
  play: (transition: FlipTransition<T, E>) => void
}

export const emptyValue = Symbol()

export type FlipTransitionController<T, E = AnimatableElement> = {
  element: E | null
  firstValue: T | typeof emptyValue
  lastValue: T | typeof emptyValue
  onSetup?: () => void
  onStart?: () => void
  onEnd?: () => void
  ref: (e: E | null) => void
}

export type FlipTransition<T, E> = {
  element: E
  firstValue: T
  lastValue: T
  onSetup?: () => void
  onStart?: () => void
  onEnd?: () => void
}

export function useFlipTransition<T, E>(
  effect: FlipEffect<T, E>,
  deps?: Array<unknown>,
): FlipTransitionController<T, E> {
  const refCallback = useCallback((e: E | null) => {
    if (e) {
      controllerRef.current.element = e
      controllerRef.current.firstValue = effect.collect(e)
    } else {
      controllerRef.current.firstValue = emptyValue
      controllerRef.current.lastValue = emptyValue
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  const controllerRef = useRef<FlipTransitionController<T, E>>({
    element: null,
    firstValue: emptyValue,
    lastValue: emptyValue,
    ref: refCallback,
  })

  useLayoutEffect(() => {
    const controller = controllerRef.current
    if (controller.element) {
      if (controller.firstValue === emptyValue) {
        controller.firstValue = effect.collect(controller.element)
      } else {
        controller.lastValue = effect.collect(controller.element)
        effect.play({
          element: controller.element,
          firstValue: controller.firstValue,
          lastValue: controller.lastValue,
          onSetup: controller.onSetup,
          onStart: controller.onStart,
          onEnd: controller.onEnd,
        })
        controller.firstValue = controller.lastValue
      }
    } else {
      controller.firstValue = emptyValue
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, deps)

  return controllerRef.current
}
