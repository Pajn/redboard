import * as React from "react"
import { ReactNode, useEffect } from "react"
import FocusLock from "react-focus-lock"
import { useTheme } from "styled-components"
import { useEnterLeaveTransition } from "./transitions/useEnterLeaveTransition"

export type DialogProps = {
  open: boolean
  onClose: () => void
  children: (childrenProps: {
    ref: (e: HTMLElement | null) => void
    style: React.CSSProperties
  }) => ReactNode
}

export const Dialog = ({ open, onClose, children }: DialogProps) => {
  const theme = useTheme()

  const backdropController = useEnterLeaveTransition(
    { in: open },
    (e, controller) => {
      e.style.backgroundColor = `rgba(0, 0, 0, ${controller.in ? 0.3 : 0})`
      e.style.opacity = controller.in ? "1" : "0"
    },
  )
  const cardController = useEnterLeaveTransition(
    { in: open },
    (e, controller) => {
      e.style.transform = controller.in ? "" : "translateY(100px) scale(0.9)"
    },
  )

  const isClosed = !backdropController.mount && !cardController.mount

  useEffect(() => {
    document.documentElement.style.overflow = isClosed ? "" : "hidden"
  }, [isClosed])

  if (isClosed) {
    return null
  }

  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,

        display: "flex",
        justifyContent: "center",
        paddingTop: 92,
        paddingBottom: 92,
        zIndex: 100,

        transition: "opacity 100ms, background-color 300ms",
      }}
      ref={backdropController.ref}
      onClick={e => {
        if (e.target === e.currentTarget) {
          onClose()
        }
      }}
      onKeyDown={
        open
          ? e => {
              if (e.key === "Escape") {
                e.stopPropagation()
                e.preventDefault()
                onClose()
              }
            }
          : undefined
      }
    >
      <FocusLock lockProps={{ style: { pointerEvents: "none" } }}>
        {children({
          ref: cardController.ref,
          style: {
            borderRadius: 10,
            backgroundColor: theme.palette.background.area,
            boxShadow:
              "rgba(0, 0, 0, 0.2) 0px 5px 5px -3px, rgba(0, 0, 0, 0.14) 0px 8px 10px 1px, rgba(0, 0, 0, 0.12) 0px 3px 14px 2px",

            pointerEvents: "auto",

            transition: "transform 150ms",
          },
        })}
      </FocusLock>
    </div>
  )
}
