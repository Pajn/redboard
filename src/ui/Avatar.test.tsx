import { expect } from "@esm-bundle/chai"
import type { User } from "../types"
import { getShortLabel } from "./Avatar"

describe("Avatar", () => {
  describe("getShortLabel", () => {
    it(`should get the name if it's short`, () => {
      expect(getShortLabel({ name: "1" } as User)).to.equal("1")
      expect(getShortLabel({ name: "12" } as User)).to.equal("12")
      expect(getShortLabel({ name: "123" } as User)).to.equal("123")
    })

    it(`should get the username if it's short and the name is long`, () => {
      expect(getShortLabel({ name: "1234", username: "1" } as User)).to.equal("1")
      expect(getShortLabel({ name: "1234", username: "12" } as User)).to.equal("12")
      expect(getShortLabel({ name: "1234", username: "123" } as User)).to.equal(
        "123",
      )
    })

    it(`should not get the username if it's missing`, () => {
      expect(getShortLabel({ name: "1234" } as User)).to.equal("1")
      expect(getShortLabel({ name: "1234", username: "" } as User)).to.equal("1")
    })

    it(`should use the initials if both name and username is long`, () => {
      expect(
        getShortLabel({ name: "Foobar", username: "foobar" } as User),
      ).to.equal("F")
      expect(
        getShortLabel({ name: "Foo Bar", username: "foobar" } as User),
      ).to.equal("FB")
      expect(
        getShortLabel({ name: "Foo Bar Baz", username: "foobar" } as User),
      ).to.equal("FBB")
      expect(
        getShortLabel({ name: "Foo Bar Baz Buz", username: "foobar" } as User),
      ).to.equal("FBB")
      // Multiple spaces does not generate "empty initials"
      expect(
        getShortLabel({ name: "Foo  Bar", username: "foobar" } as User),
      ).to.equal("FB")
      // Surrogate pairs does not get separated
      expect(
        getShortLabel({ name: "𝛀 is omega", username: "omega" } as User),
      ).to.equal("𝛀io")
    })
  })
})
