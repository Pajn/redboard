import { projectContext } from "@app/stores/ProjectStore"
import { centerContent } from "@app/styles"
import type { User } from "@app/types"
import { enumerate, find } from "iterates/sync"
import { pipeValue } from "iterates/utils"
import * as React from "react"
import styled from "styled-components"

export function getShortLabel(user: User) {
  if (user.name.length <= 3) return user.name
  if ((user.username?.length ?? 0) >= 1 && (user.username?.length ?? 0) <= 3) {
    return user.username
  }

  return user.name
    .split(" ")
    .filter(Boolean)
    .map(name => String.fromCodePoint(name.codePointAt(0)!))
    .slice(0, 3)
    .join("")
}

const colors = [
  "#038154", // green 600
  "#6a27b9", // purple 600
  "#8d232b", // red 700
  "#04444e", // kale 600
  "#d52054", // pink 600
  "#3253e3", // royal 600
  "#a81898", // fushcia 600
  "#ed961b", // yellow 600
  "#c44f00", // orange 600
  "#1f73b7", // blue 600
  "#c72a1b", // crimson 600
  "#02807a", // teal 600
  "#068541", // mint 600
]

const noUserColor = "#333333"

export type AvatarProps = {
  user?: User
  description?: string
  size?: number
  button?: boolean
  hidden?: boolean
} & Omit<React.HTMLProps<HTMLDivElement | HTMLButtonElement>, "ref" | "as">

export const Avatar = React.forwardRef<HTMLElement, AvatarProps>(
  function Avatar(
    { user, description, size = 28, button, hidden, style, children, ...props },
    ref,
  ) {
    const color = projectContext.useStateSlice(
      project =>
        user
          ? pipeValue(
              project.members.values(),
              enumerate,
              find(m => m.item.id === user.id),
              m => (m ? colors[m.index % colors.length] : noUserColor),
            )
          : noUserColor,
      { deps: [user?.id] },
    )
    const Container = (button
      ? AvatarButtonContainer
      : AvatarContainer) as typeof AvatarContainer

    const label = user ? getShortLabel(user) : undefined

    return (
      <Container
        ref={ref as any}
        title={
          description
            ? `${description}: ${user ? user.name : "nobody"}`
            : user?.name
        }
        {...props}
        haveUser={user != null}
        hidden={hidden}
        style={{
          ...centerContent,
          width: size,
          height: size,
          fontSize: Math.floor(size * (label?.length === 3 ? 0.42 : 0.46)),
          background: color,
          cursor: button ? "pointer" : undefined,
          ...style,
        }}
      >
        {label}
        {children}
      </Container>
    )
  },
)

const AvatarContainer = styled.div(
  (props: { haveUser: boolean; hidden?: boolean }) => ({
    position: "relative" as const,
    flexShrink: 0,
    padding: 0,
    border: `thin ${props.haveUser ? "solid" : "dashed"} white`,
    borderRadius: "50%",
    textTransform: "uppercase" as const,
    color: "white",
    outline: "none",
    opacity: props.hidden ? 0 : 1,
    transition: `opacity 100ms ease-out`,

    "&:hover": {
      opacity: 1,
      transitionDuration: "200ms",
      transitionTimingFunction: "ease-in",
    },
    "&.focus-visible": {
      opacity: 1,
      transitionDuration: "200ms",
      transitionTimingFunction: "ease-in",
    },
  }),
)
const AvatarButtonContainer = AvatarContainer.withComponent("button")
