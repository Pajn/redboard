import React, { CSSProperties, useEffect, useState } from "react"
import styled, { keyframes } from "styled-components"

const fade = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`

const ripple = keyframes`
  0%,
  80%,
  100% {
    transform: scale(0)
  }
  40% {
    transform: scale(1)
  }
  50% {
    transform: scale(1)
  }
`

const RippleContainer = styled.div`
  position: relative;
  width: 50px;
  color: transparent;
  opacity: 1;
  animation: ${fade} 400ms cubic-bezier(0, 0.2, 0.8, 1);
  transition: opacity 400ms cubic-bezier(0, 0.2, 0.8, 1);
`
const RippleItem = styled.div`
  position: absolute;
  top: calc(50% - 5px);
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background: white;
  animation: ${ripple} 1.8s infinite ease-in-out;
  animation-fill-mode: both;
  will-change: transform;
`

export type LoadingSpinnerProps = {
  loading: boolean
  size?: number
  delay?: number
  style?: CSSProperties
}

export const LoadingSpinner = ({
  loading,
  delay = 200,
  style,
}: LoadingSpinnerProps) => {
  const [spinning, setSpinning] = useState(false)
  const [fading, setFading] = useState(false)
  const render = spinning || fading

  useEffect(() => {
    if (loading && !spinning) {
      if (fading) {
        setSpinning(true)
        setFading(false)
      } else {
        const timeout = setTimeout(() => {
          setSpinning(true)
        }, delay)

        return () => clearTimeout(timeout)
      }
    }
    if (!loading && spinning && !fading) {
      setFading(true)
      setSpinning(false)
    }
  }, [delay, fading, loading, spinning])

  return render ? (
    <RippleContainer
      style={{
        position: "relative",
        opacity: fading ? 0 : 1,
        ...style,
      }}
      onTransitionEnd={() => {
        if (fading) setFading(false)
      }}
    >
      Loading...
      <RippleItem style={{ animationDelay: "-320ms" }} />
      <RippleItem style={{ left: 20, animationDelay: "-160ms" }} />
      <RippleItem style={{ left: 40 }} />
    </RippleContainer>
  ) : null
}
