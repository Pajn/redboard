import { useEffect, useRef, useState } from "react"

export function usePageVisibility(cb: () => void) {
  const cbRef = useRef(cb)
  cbRef.current = cb
  const [handler] = useState(() => () => {
    cbRef.current()
  })

  useEffect(() => {
    document.addEventListener("visibilitychange", handler, false)
    return () => document.removeEventListener("visibilitychange", handler)
  }, [handler])
}
