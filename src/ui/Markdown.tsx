import { RedmineStore, redmineContext } from "@app/stores/RedmineStore"
import * as React from "react"
import markdown from "remark-parse/index"
import styled from "styled-components"
import unified from "unified/index"

function locateIssueReference(value: string, fromIndex: number) {
  return value.indexOf("#", fromIndex)
}

function issueReferences(
  this: unified.Processor,
  options: {
    issueDescription: (issueId: number) => { url: string; title: string }
  },
) {
  const Parser = this.Parser
  const tokenizers = Parser.prototype.inlineTokenizers
  const methods = Parser.prototype.inlineMethods

  tokenizers.mention = tokenizeIssueReference

  function tokenizeIssueReference(eat: any, value: string, silent: boolean) {
    const match = /^#(\d+)/.exec(value)

    if (match) {
      if (silent) {
        return true
      }

      const { url, title } = options.issueDescription(+match[1])

      return eat(match[0])({
        type: "link",
        target: "redmine",
        url,
        title,
        children: [{ type: "text", value: match[0] }],
      })
    }
  }

  tokenizeIssueReference.notInLink = true
  tokenizeIssueReference.locator = locateIssueReference

  methods.splice(methods.indexOf("text"), 0, "mention")
}

const MarkdownStyles = styled.div(({ theme }) => ({
  p: {
    marginBottom: "0.5em",

    "&:first-child": {
      marginTop: 0,
    },
    "&:last-child": {
      marginBottom: 0,
    },
  },
  img: {
    maxWidth: "100%",
  },
  blockquote: {
    margin: 8,
    padding: "4px 8px",
    borderLeft: `4px solid ${theme.palette.divider.strong}`,
  },
  code: {
    fontFamily: "monospace",
    "&.inline": {
      color: "hsl(0, 70%, 70%)",
    },
    "&.block": {
      display: "block",
      padding: "4px 8px",
      fontFamily: "monospace",
      whiteSpace: "pre-wrap",
      borderRadius: 4,
      backgroundColor: theme.palette.background.above,
    },
  },
  "ul, ol": {
    marginTop: "0.5em",
    marginBottom: "0.5em",
    paddingLeft: 24,

    "ul, ol": {
      margin: 0,
    },
  },
  li: {
    "p:last-of-type": {
      marginBottom: 0,
    },
  },
  table: {
    borderCollapse: "collapse",

    "tbody>tr:nth-child(2n + 1)": {
      backgroundColor: theme.palette.background.above,
    },

    th: {
      borderBottom: `1px solid ${theme.palette.divider.strong}`,
      fontWeight: "bold",

      ":not([align])": {
        textAlign: "left",
      },
    },

    "th, td": {
      padding: "3px 6px",
    },

    "th + th, td + td": {
      borderLeft: `1px solid ${theme.palette.divider.strong}`,
    },
  },
}))

const components: Record<string, React.ComponentType | string> = {
  root: MarkdownStyles,
  thematicBreak: "hr",
  paragraph: "p",
  strong: "strong",
  emphasis: "em",
  delete: "del",
  heading: ({ depth, children }: any) =>
    React.createElement(`h${depth}`, { children }),
  link: ({ url, title, target, children }: any) => (
    <a href={url} title={title} target={target}>
      {children}
    </a>
  ),
  image: ({ url, title, alt }: any) => (
    <img src={url} title={title} alt={alt} />
  ),
  inlineCode: ({ children }) => <code className="inline">{children}</code>,
  code: ({ children }) => <code className="block">{children}</code>,
  blockquote: "blockquote",
  list: ({ ordered, children }: any) =>
    React.createElement(ordered ? "ol" : "ul", { children }),
  listItem: "li",
  table: ({ align, rawChildren: [thead, ...tbody] }: any) => {
    return (
      <table>
        <thead>
          <tr>
            {thead.children.map((column: any, i: number) =>
              React.createElement(
                "th",
                { key: i, align: align[i] },
                ...column.children.map(render),
              ),
            )}
          </tr>
        </thead>
        <tbody>
          {tbody.map((row: any, i: number) => (
            <tr key={i}>
              {row.children.map((column: any, i: number) =>
                React.createElement(
                  "td",
                  { key: i, align: align[i] },
                  ...column.children.map(render),
                ),
              )}
            </tr>
          ))}
        </tbody>
      </table>
    )
  },
  text: ({ children }) => <>{children}</>,
}

function render(node: any) {
  const { type, position: _, value, children: rawChildren, ...props } = node
  const children =
    rawChildren !== undefined
      ? rawChildren.map(render)
      : value === undefined
      ? []
      : [value]
  return typeof components[type] === "function" ||
    typeof components[type] === "object"
    ? React.createElement(
        components[type],
        { rawChildren, ...props } as any,
        ...children,
      )
    : typeof components[type] === "string"
    ? React.createElement(components[type], {}, ...children)
    : null
}

function parse(text: string, redmineStore: RedmineStore) {
  const ast = unified()
    .use(markdown, { commonmark: true })
    .use(issueReferences, {
      issueDescription: (issueId) => {
        const issue = redmineStore
          .getState()
          .projectStore?.getState()
          .boardStore?.getState()
          .issues.get(issueId)

        return {
          url: redmineStore.formatIssueUrl(issueId),
          title: issue
            ? `${issue.tracker.name}: ${issue.subject} (${issue.status.name})`
            : "",
        }
      },
    })
    .parse(text)

  return ast
}

export const Markdown = (props: {
  text: string
  style?: React.CSSProperties
}) => {
  const redmineStore = redmineContext.useStore()
  return React.useMemo(() => {
    const element = render(parse(props.text, redmineStore))

    return React.cloneElement(element!, { style: props.style } as any)
  }, [props.text, props.style, redmineStore])
}
