import type * as PopperJs from "@popperjs/core"
import applyStyles from "@popperjs/core/lib/modifiers/applyStyles"
import computeStyles from "@popperjs/core/lib/modifiers/computeStyles"
import eventListeners from "@popperjs/core/lib/modifiers/eventListeners"
import flip from "@popperjs/core/lib/modifiers/flip"
import popperOffsets from "@popperjs/core/lib/modifiers/popperOffsets"
import { createPopper } from "@popperjs/core/lib/popper-base"
import Portal from "@reach/portal"
import * as React from "react"
import { ownerWindow, setRef, useForkRef } from "./utils"

export type PopperPlacementType =
  | "bottom-end"
  | "bottom-start"
  | "bottom"
  | "left-end"
  | "left-start"
  | "left"
  | "right-end"
  | "right-start"
  | "right"
  | "top-end"
  | "top-start"
  | "top"

type ChildProps = {
  placement: PopperPlacementType
}

export type PopperProps = React.HTMLAttributes<HTMLDivElement> & {
  anchorEl?:
    | null
    | Element
    | PopperJs.VirtualElement
    | (() => Element | PopperJs.VirtualElement)
  children: React.ReactElement | ((props: ChildProps) => React.ReactNode)
  keepMounted?: boolean
  open: boolean
  placement?: PopperPlacementType
  modifiers?: Array<PopperJs.Modifier<any, any>>
  popperRef?: React.Ref<PopperJs.Instance | null>
}

function getAnchorEl(anchorEl: PopperProps["anchorEl"]) {
  return typeof anchorEl === "function" ? anchorEl() : anchorEl
}

const useEnhancedEffect =
  typeof window !== "undefined" ? React.useLayoutEffect : React.useEffect

export const Popper = React.forwardRef<unknown, PopperProps>(function Popper(
  props,
  ref,
) {
  const {
    anchorEl,
    children,
    keepMounted = false,
    open,
    placement = "bottom",
    modifiers = [],
    popperRef: popperRefProp,
    ...other
  } = props
  const tooltipRef = React.useRef<HTMLElement | null>(null)
  const ownRef = useForkRef(tooltipRef, ref)

  const popperRef = React.useRef<PopperJs.Instance | null>(null)
  const handlePopperRef = useForkRef(popperRef, popperRefProp)
  const handlePopperRefRef = React.useRef(handlePopperRef)
  useEnhancedEffect(() => {
    handlePopperRefRef.current = handlePopperRef
  }, [handlePopperRef])
  React.useImperativeHandle(popperRefProp, () => popperRef.current, [])

  React.useEffect(() => {
    if (popperRef.current) {
      popperRef.current.update()
    }
  })

  const handleOpen = React.useCallback(() => {
    if (!tooltipRef.current || !anchorEl || !open) {
      return
    }

    if (popperRef.current) {
      popperRef.current.destroy()
      if (handlePopperRefRef.current) {
        handlePopperRefRef.current(null)
      }
    }

    const resolvedAnchorEl = getAnchorEl(anchorEl)

    if (import.meta.env.MODE !== "production") {
      const containerWindow = ownerWindow(resolvedAnchorEl)

      if (resolvedAnchorEl instanceof (containerWindow as any).Element) {
        const box = (resolvedAnchorEl as Element).getBoundingClientRect()

        if (
          import.meta.env.MODE !== "test" &&
          box.top === 0 &&
          box.left === 0 &&
          box.right === 0 &&
          box.bottom === 0
        ) {
          console.warn(
            [
              "Material-UI: the `anchorEl` prop provided to the component is invalid.",
              "The anchor element should be part of the document layout.",
              "Make sure the element is present in the document or that it's not display none.",
            ].join("\n"),
          )
        }
      }
    }

    const popper = createPopper(
      resolvedAnchorEl || document.body,
      tooltipRef.current,
      {
        placement,

        modifiers: [
          applyStyles,
          computeStyles,
          eventListeners,
          popperOffsets,
          flip,
          ...modifiers,
        ],
      },
    )
    if (handlePopperRefRef.current) {
      handlePopperRefRef.current(popper)
    }
  }, [anchorEl, modifiers, open, placement])

  const handleRef = React.useCallback(
    (node) => {
      setRef(ownRef, node)
      handleOpen()
    },
    [ownRef, handleOpen],
  )

  const handleClose = () => {
    if (!popperRef.current) {
      return
    }

    popperRef.current.destroy()
    if (handlePopperRefRef.current) {
      handlePopperRefRef.current(null)
    }
  }

  React.useEffect(() => {
    // Let's update the popper position.
    handleOpen()
  }, [handleOpen])

  React.useEffect(() => {
    return () => {
      handleClose()
    }
  }, [])

  React.useEffect(() => {
    if (!open) {
      handleClose()
    }
  }, [open])

  if (!keepMounted && !open) {
    return null
  }

  const childProps: ChildProps = { placement }

  return (
    <Portal>
      <div
        ref={handleRef}
        role="tooltip"
        {...other}
        style={{
          // Prevents scroll issue, waiting for Popper.js to add this style once initiated.
          position: "fixed",
          // Fix Popper.js display issue
          top: 0,
          left: 0,
          ...other.style,
        }}
      >
        {typeof children === "function" ? children(childProps) : children}
      </div>
    </Portal>
  )
})
