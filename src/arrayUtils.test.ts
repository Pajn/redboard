import { expect } from "@esm-bundle/chai"
import { ArrayUtils } from "./arrayUtils"

describe("ArrayUtils", () => {
  describe(".insert", () => {
    it("should insert an item at specified position", () => {
      const colors = ["red", "green", "blue"]

      expect(ArrayUtils.insert(colors, 0, "orange")).to.deep.equal([
        "orange",
        "red",
        "green",
        "blue",
      ])

      expect(ArrayUtils.insert(colors, 1, "orange")).to.deep.equal([
        "red",
        "orange",
        "green",
        "blue",
      ])

      expect(ArrayUtils.insert(colors, 2, "orange")).to.deep.equal([
        "red",
        "green",
        "orange",
        "blue",
      ])

      expect(ArrayUtils.insert(colors, 3, "orange")).to.deep.equal([
        "red",
        "green",
        "blue",
        "orange",
      ])

      expect(colors).to.deep.equal(["red", "green", "blue"])
    })
  })

  describe(".move", () => {
    it("should move in item to a new position", () => {
      const colors = ["red", "green", "blue"]

      expect(ArrayUtils.move(colors, 0, 1)).to.deep.equal([
        "green",
        "red",
        "blue",
      ])
      expect(ArrayUtils.move(colors, 0, 2)).to.deep.equal([
        "green",
        "blue",
        "red",
      ])
      expect(ArrayUtils.move(colors, 2, 0)).to.deep.equal([
        "blue",
        "red",
        "green",
      ])
      expect(ArrayUtils.move(colors, 1, 0)).to.deep.equal([
        "green",
        "red",
        "blue",
      ])
      expect(ArrayUtils.move(colors, 2, 1)).to.deep.equal([
        "red",
        "blue",
        "green",
      ])

      expect(colors).to.deep.equal(["red", "green", "blue"])
    })
  })

  describe(".updateWhere", () => {
    it("should update items matching the predicate", () => {
      const colors = ["red", "green", "blue"]

      expect(
        ArrayUtils.updateWhere(
          colors,
          color => color.length === 4,
          color => color.toUpperCase(),
        ),
      ).to.deep.equal(["red", "green", "BLUE"])
      expect(
        ArrayUtils.updateWhere(
          colors,
          color => color.length <= 4,
          color => color.toUpperCase(),
        ),
      ).to.deep.equal(["RED", "green", "BLUE"])
      expect(
        ArrayUtils.updateWhere(
          colors,
          color => color.length > 5,
          color => color.toUpperCase(),
        ),
      ).to.deep.equal(["red", "green", "blue"])

      expect(colors).to.deep.equal(["red", "green", "blue"])
    })
  })

  describe(".upsertWhere", () => {
    it("should update items matching the predicate", () => {
      const colors = ["red", "green", "blue"]

      expect(
        ArrayUtils.upsertWhere(
          colors,
          color => color.length === 4,
          "yellow",
          color => color.toUpperCase(),
        ),
      ).to.deep.equal(["red", "green", "BLUE"])
      expect(
        ArrayUtils.upsertWhere(
          colors,
          color => color.length <= 4,
          "yellow",
          color => color.toUpperCase(),
        ),
      ).to.deep.equal(["RED", "green", "BLUE"])
      expect(
        ArrayUtils.upsertWhere(
          colors,
          color => color.length > 5,
          "yellow",
          color => color.toUpperCase(),
        ),
      ).to.deep.equal(["red", "green", "blue", "YELLOW"])
      expect(
        ArrayUtils.upsertWhere<string | undefined, string>(
          colors,
          color => color.length > 5,
          undefined,
          (color = "yellow") => color.toUpperCase(),
        ),
      ).to.deep.equal(["red", "green", "blue", "YELLOW"])

      expect(colors).to.deep.equal(["red", "green", "blue"])
    })
  })
})
