import type { Map } from "immutable"

export function assertType<T>(item: T) {
  return item
}

export type Clearable<T> = {
  [K in keyof T]: undefined extends T[K] ? T[K] | null : T[K]
}

export type IssueId = number
export type ProjectId = number
export type UserId = number
export type StatusName = string

export type Project = {
  id: ProjectId
  name: string
  slug: string
  columns: Array<Status>
  destructiveStatuses: Array<Status>
  members: Map<UserId, User>
  trackers: Array<Tracker>
  versions: Array<Version>
}

export type BoardStructure = ReadonlyArray<readonly [Issue, SwimlaneColumns]>
export type SwimlaneColumns = Map<StatusName, ReadonlyArray<IssueId>>

export type Board = {
  structure: BoardStructure
  issues: Map<IssueId, Issue>
}

export type Issue = {
  id: IssueId
  projectId: ProjectId
  tracker: Tracker
  subject: string
  description: string
  priority: Pick<Priority, "id" | "name">
  status: Pick<Status, "id" | "name">
  parent?: IssueId
  version?: Pick<Version, "id" | "name">
  assignee?: UserId
  attachments: Array<Attachment>
  customFields: Array<CustomField>
  createdOn: Date
  createdBy: User
  updatedOn: Date
}

export type Attachment = {
  id: number
  filename: string
  filesize: number
  contentType: string
  description: string
  contentUrl: string
  thumbnailUrl: string
  author: User
  createdOn: Date
}

export type CustomField = { id: number; name: string; value: string | null }

export type NewIssue = Omit<
  Issue,
  | "id"
  | "createdOn"
  | "createdBy"
  | "updatedOn"
  | "customFields"
  | "attachments"
>

export type FullIssue = Issue & {
  comments: Array<HistoryEvent>
  history: Array<HistoryEvent>
  children: Array<Issue>
}

export type ChangedPropertyKey = keyof Omit<
  Issue,
  | "id"
  | "createdOn"
  | "createdBy"
  | "updatedOn"
  | "attachments"
  | "customFields"
>

export type ChangedProperty<
  P extends ChangedPropertyKey = ChangedPropertyKey
> =
  | { action: "added"; property: P; newValue: Required<Issue>[P] }
  | {
      action: "updated"
      property: P
      oldValue: Required<Issue>[P]
      newValue: Required<Issue>[P]
    }
  | { action: "removed"; property: P; oldValue: Required<Issue>[P] }

export type ChangedPropertyValue<
  P extends ChangedPropertyKey = ChangedPropertyKey
> = { property: P; value: Required<Issue>[P] }

export type HistoryEvent = {
  id: number
  createdOn: Date
  user: User
  notes: string
  changes: Array<
    | { type: "createdIssue" }
    | {
        type: "attachment"
        action: "added" | "removed"
        id: number
        filename: string
      }
    | ({ type: "property" } & ChangedProperty)
  >
}

export type Priority = {
  id: number
  name: string
  isDefault: boolean
  active: boolean
}

export type Status = {
  id: number
  name: StatusName
  isClosed: boolean
}

export type User = {
  id: UserId
  username?: string
  name: string
}

export type Tracker = {
  id: number
  name: string
}

export type Version = {
  id: number
  name: string
  isOpen: boolean
}
