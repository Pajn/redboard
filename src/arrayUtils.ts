import { enumerate } from "iterates/sync"

export const ArrayUtils = {
  insert<T>(items: ReadonlyArray<T>, index: number, item: T) {
    const newItems = [...items]
    newItems.splice(index, 0, item)
    return newItems
  },

  move<T>(items: ReadonlyArray<T>, fromIndex: number, toIndex: number) {
    const newItems = [...items]
    newItems.splice(fromIndex, 1)
    newItems.splice(toIndex, 0, items[fromIndex])
    return newItems
  },

  updateWhere<T>(
    items: ReadonlyArray<T>,
    predicate: (item: T) => boolean,
    updater: (item: T) => T,
  ) {
    let newItems = items as Array<T>

    for (const { index, item } of enumerate(items)) {
      if (predicate(item)) {
        if (newItems === items) {
          newItems = [...items]
        }

        newItems[index] = updater(item)
      }
    }

    return newItems
  },

  upsertWhere<T, U extends T = T>(
    items: ReadonlyArray<U>,
    predicate: (item: U) => boolean,
    defaultValue: T,
    updater: (item: T) => U,
  ) {
    const newItems = [...items]
    let foundValue = false

    for (const { index, item } of enumerate(newItems)) {
      if (predicate(item)) {
        foundValue = true

        newItems[index] = updater(item)
      }
    }

    if (!foundValue) {
      newItems.push(updater(defaultValue))
    }

    return newItems
  },
}
